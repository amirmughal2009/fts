import express from 'express';
import verificationController from '../controllers/verification_controller';

const router = express.Router();

router.route('/')
  // POST /tests
  .post(verificationController.verify)

module.exports = router;