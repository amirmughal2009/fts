import User from '../models/users';

const verificationControllers = {

  verify: async (req, res, next) => {

    const { type, email, eCode, phoneNo, pCode } = req.body;
    
    if(type === 'email') {

      const user = await User.findOne({ email, eCode });
      if(user) {
        return res.send({ data: user, status: true, msg: 'Email verified' });
      } else {
        return res.send({ status: false, data: {}, msg: 'Email not verified' });
      }
      
    } else {
      const user = await User.findOne({ phoneNo, pCode });
      if(user) {
        return res.send({ data: user, status: true, msg: 'Phone no verified' });
      } else {
        return res.send({ status: false, data: {}, msg: 'Phone no not verified' });
      }
    }
  }
}

export default verificationControllers;
