import { gql } from 'apollo-server-express';

const Mutations = gql`
  extend type Mutation {
    registerUser(input: registerUserInputs): Response
    loginUser(phoneNo: String!, password: String!): Response
    forgotPassword(email: String!, phoneNo: String!): Response
    resetPassword(password: String!, retypePassword: String!, phoneNo: String!): Response
  }

  input registerUserInputs {
    fullName: String!
    email: String!
    password: String!
    confirmPassword: String!
    phoneNo: String!
    dateOfBirth: String!
    gender: String!
  }
`;

export default Mutations;
