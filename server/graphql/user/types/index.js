import { gql } from 'apollo-server-express';

import Mutations from './mutations';
import Queries from './queries';

const User = gql`
  type User {
    _id: ID,
    fullName: String
    email: String!
    password: String!
    confirmPassword: String!
    phoneNo: String
    dateOfBirth: String
    gender: String
    token: String
  }
  type Count {
    count: ID!
  }
`;

export default [User, Mutations, Queries];
