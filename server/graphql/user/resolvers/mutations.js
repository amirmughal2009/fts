import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { SECRET_KEY, SALT_ROUNDS } from '../../../../config/constants';
import User from '../../../models/users';

const registerUser = async (parent, args) => {
  try {

    const { email, phoneNo, password, confirmPassword } = args.input;
    const isExist = await User.findOne({ $or:[{ email }, { phoneNo }] });

    if(isExist) return { status: false, data: {}, msg: 'User already registered !!!' };

    args.input.password = await bcrypt.hash(password, SALT_ROUNDS);
    args.input.confirmPassword = await bcrypt.hash(confirmPassword, SALT_ROUNDS);
    args.input.eCode = '1234';
    args.input.pCode= '1234';
    const user = await User.create(args.input);
    if (user) {
      return { status: true, data: { user }, msg: 'user registered successfully!' };
    }
    return { status: false, data: { user }, msg: 'user not registered!' };
  } catch (error) {
    return { status: false, data: { user: null }, msg: error.message };
  }
};

const loginUser = async (parent, args) => {
  try {
    const user = await User.findOne({ phoneNo: args.phoneNo });
    if (!user) {
      return { status: false, data: { user: args }, msg: 'Please enter corrent phone number/password' };
    }
    const passwordVerify = await bcrypt.compare(args.password, user.password);
    if (!passwordVerify) {
      return { status: false, data: { user: args }, msg: 'Please enter corrent phone number/password' };
    }
    const token = jwt.sign({ email: user.email, password: args.password }, SECRET_KEY);
    user.token = token;
    const updatedUser = await User.findOneAndUpdate({ email: user.email }, user);
    if (!updatedUser) {
      return { status: false, data: { user: args }, msg: 'Please enter corrent phone number/password' };
    }
    return { status: true, data: { user: updatedUser }, msg: 'loggedin successfully !' };
  } catch (error) {
    return { status: false, data: null, msg: error.message };
  }
};

const forgotPassword = async (parent, args) => {
  try {
    const user = await User.findOne({$or: [{ phoneNo: args.phoneNo }, { email: args.email }]});
    if (!user) {
      return { status: false, data: { forgotType: 'not found' }, msg: 'Please enter corrent phone number/password' };
    }

    const userType = await User.findOne({ phoneNo: args.phoneNo });
    if (!userType) {
      return { status: true, data: { forgotType: 'email sent sucessfully' }, msg: 'user found' };
    }
    return { status: true, data: { forgotType: 'message sent successfully' }, msg: 'user found!' };
  } catch (error) {
    return { status: false, data: { forgotType: null }, msg: error.message };
  }
};

const resetPassword = async (parent, args) => {
  try {
    const user = await User.findOne({ phoneNo: args.phoneNo });
    if (!user) {
      return { status: false, data: { user: args }, msg: 'Please enter corrent phone number/password' };
    }
    user.password = await bcrypt.hash(args.password, SALT_ROUNDS);
    const updatedUser = await User.findOneAndUpdate({ email: user.email }, user);
    return { status: true, data: { reset: 'password updated successfully' }, msg: 'password updated!' };
  } catch (error) {
    return { status: false, data: { reset: null }, msg: error.message };
  }
};

export default { loginUser, registerUser, forgotPassword, resetPassword };
