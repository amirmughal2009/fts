import { gql } from 'apollo-server-express';

const response = gql`
  type Data {
    user: User
    book: Book
    forgotType : String
    reset : String
  }

  type Response {
    data: Data
    status: Boolean
    msg: String
  }
`;

export default response;
