import React, { Component } from 'react';
import { Button, Modal } from 'semantic-ui-react';

export const RemoveModalBox = ({ size, open, close, name }) => {
  return (
    <Modal size={size} open={open} onClose={close}>
      <Modal.Content>
        <p>{name}</p>
      </Modal.Content>
      <Modal.Actions>
        <Button negative>No</Button>
        <Button positive icon='checkmark' labelPosition='right' content='Yes' />
      </Modal.Actions>
    </Modal>
  )
}