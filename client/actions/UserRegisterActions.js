import USER_REGISTER from '../graphql/mutations/user/register';

const UserRegisterAction = (client, variables) => {
  return (dispatch) => {
    dispatch({ type: 'USER_REGISTER' });
    client.mutate({
      mutation: USER_REGISTER,
      variables
    }).then((response) => {
      dispatch({ type: 'USER_REGISTER_FULFILLED', payload: response.data });
    }).catch((err) => {
      dispatch({ type: 'USER_REGISTER_REJECTED', payload: err });
    });
  };
};

export default { UserRegisterAction };
