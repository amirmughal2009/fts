import RESET_PASSWORD from '../graphql/mutations/user/resetPassword';

const resetPasswordAction = (client, userInputs) => {
  return (dispatch) => {
    dispatch({ type: 'RESET_PASSWORD' });
    console.log('dispatch');
    client.mutate({
      mutation: RESET_PASSWORD,
      variables: userInputs
    }).then((response) => {
      dispatch({ type: 'RESET_PASSWORD_FULFILLED', payload: response.data });
    }).catch((err) => {
      dispatch({ type: 'RESET_PASSWORD_REJECTED', payload: err });
    });
  };
};

export default { resetPasswordAction };
