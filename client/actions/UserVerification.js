const UserVerificationAction = (variables) => {
  return (dispatch) => {
    dispatch({ type: 'USER_VERIFICATION' });
    fetch('api/v1/verification/', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        ...variables
      })
    }).then(response => response.json())
    .then(data => {
      dispatch({ type: 'USER_VERIFICATION_FULFILLED', payload: data });
    }).catch((err) => {
      dispatch({ type: 'USER_VERIFICATION_REJECTED', payload: err });
    });
  };
};

export default { UserVerificationAction };
