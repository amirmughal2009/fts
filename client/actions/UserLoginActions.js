import USER_LOGIN from '../graphql/mutations/user/login';

const UserLoginAction = (client, userInputs) => {
  return (dispatch) => {
    dispatch({ type: 'USER_LOGIN' });
    client.mutate({
      mutation: USER_LOGIN,
      variables: userInputs
    }).then((response) => {
      dispatch({ type: 'USER_LOGIN_FULFILLED', payload: response.data });
    }).catch((err) => {
      dispatch({ type: 'USER_LOGIN_REJECTED', payload: err });
    });
  };
};

export default { UserLoginAction };
