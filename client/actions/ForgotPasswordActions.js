import FORGOT_PASSWORD from '../graphql/mutations/user/forgotPassword';

const ForgotPasswordAction = (client, variables) => {
  return (dispatch) => {
    dispatch({ type: 'FORGOT_PASSWORD' });
    client.mutate({
      mutation: FORGOT_PASSWORD,
      variables
    }).then((response) => {
      dispatch({ type: 'FORGOT_PASSWORD_FULFILLED', payload: response.data });
    }).catch((err) => {
      dispatch({ type: 'FORGOT_PASSWORD_REJECTED', payload: err });
    });
  };
};

export default { ForgotPasswordAction };
