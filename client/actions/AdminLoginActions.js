import USER_LOGIN from '../graphql/mutations/user/login';

const AdminLoginAction = (client, userInputs) => {
  return (dispatch) => {
    dispatch({ type: 'ADMIN_LOGIN' });
    client.mutate({
      mutation: USER_LOGIN,
      variables: userInputs
    }).then((response) => {
      dispatch({ type: 'ADMIN_LOGIN_FULFILLED', payload: response.data });
    }).catch((err) => {
      dispatch({ type: 'SIGNUP_USER_REJECTED', payload: err });
    });
  };
};

export default { AdminLoginAction };
