import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import { store } from '../store';

//Containers
import DashboardContainer from '../container/admin/DashboardContainer.jsx';

//pages
import Login from '../pages/admin/Login.jsx';
import Dashboard from '../pages/admin/Dashboard.jsx';
import Sport from '../pages/admin/Sport.jsx';
import City from '../pages/admin/City.jsx';
// client side pages
import Splash from '../pages/client/Splash.jsx';
import ClientLogin from '../pages/client/Login.jsx';
import ClientSignup from '../pages/client/Signup.jsx';
import ResetPassword from '../pages/client/ResetPassword.jsx';
import Profile from '../pages/client/Profile.jsx';
import UserVerification from '../pages/client/UserVerification.jsx';
import ForgotPassword from '../pages/client/ForgotPassword.jsx';
import ClientHome from '../pages/client/Dashboard.jsx';
import Offers from '../pages/client/Offers.jsx';
import BooknPlay from '../pages/client/BooknPlay.jsx';
import AboutUs from '../pages/client/AboutUs.jsx';
import BooknPlayDetails from '../pages/client/BooknPlayDetails.jsx';
import PartnerWithUs from '../pages/client/PartnerWithUs.jsx';

const renderRoutes = () => {
  return (
    <Router>
      <Provider store={store}>
        <Switch>
          <Route exact path="/admin-login" render={props => <AppRoute Component={Login} props={props} />} />
          <Route exact path="/dashboard" render={props => <AppRoute Layout={DashboardContainer} Component={Dashboard} props={props} />} />
          <Route exact path="/sport" render={props => <AppRoute Layout={DashboardContainer} Component={Sport} props={props} />} />
          <Route exact path="/city" render={ props => <AppRoute Layout={DashboardContainer} Component={City} props={props} />} />
          
          <Route exact path="/" render={props => <AppRoute Component={Splash} props={props} />} />
          <Route exact path="/login" render={props => <AppRoute Component={ClientLogin} props={props} />} />
          <Route exact path="/signup" render={props => <AppRoute Component={ClientSignup} props={props} />} />
          <Route exact path="/reset-password" render={props => <AppRoute Component={ResetPassword} props={props} />} />
          <Route exact path="/verify-account" render={props => <AppRoute Component={UserVerification} props={props} />} />
          <Route exact path="/forgot-password" render={props => <AppRoute Component={ForgotPassword} props={props} />} />
          <Route exact path="/home" render={props => <AppRoute Component={ClientHome} props={props} />} />
          <Route exact path="/offers" render={props => <AppRoute Component={Offers} props={props} />} />
          <Route exact path="/booknplay" render={props => <AppRoute Component={BooknPlay} props={props} />} />
          <Route exact path="/about-us" render={props => <AppRoute Component={AboutUs} props={props} />} />
          <Route exact path="/booknplay-details" render={props => <AppRoute Component={BooknPlayDetails} props={props} />} />
          <Route exact path="/partner-with-us" render={props => <AppRoute Component={PartnerWithUs} props={props} />} />   
          <Route exact path="/profile" render={ props => <AppRoute Component={Profile} props={props} /> } />     
        </Switch>
      </Provider>
    </Router>
  );
};

const AppRoute = ({ Component, Layout, props }) => {
  if (Layout) {
    return (
      <Layout {...props}>
        <Component {...props} />
      </Layout>
    );
  }

  if (!Component) {
    return <Layout {...props} />;
  }

  return <Component {...props} />;
};

export default renderRoutes;
