import React, { Component } from 'react';
import { Header } from 'semantic-ui-react';
import JoinCity from './city/JoinCity.jsx';

class City extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div>
        <Header as="h2" disabled>
          VIEW CITY
        </Header>
        <JoinCity />
      </div>
    )
  }
}

export default City;
