import React, { Component } from 'react';
import {
  Header,
  Grid,
  Input,
  Menu,
  Icon,
  Button,
  Container,
  Dropdown
} from 'semantic-ui-react';


class Sport extends Component {
  constructor() {
    super();
    this.state = {
      categoryName: '',
      sportName: '',
      options: [],
      sportIcon: '/src/assets/images/logo/logo.png'
    }
  }

  handleClick = () => {
    const { categoryName, options } = this.state;
    if (categoryName) {
      options.push({ key: (options.length + 1), text: categoryName, value: categoryName, sports: [] });
    }
  }

  addSport = () => {
    const { categoryName, options, sportName, sportIcon } = this.state;
    const index = options.findIndex((category) => category.value === categoryName);
    options[index].sports.push({key: (options.length + 2), name: sportName, icon: sportIcon });
    this.setState({ sportName: '', options });
  }


  render() {
    const { categoryName, sportName, options } = this.state;
    return (
      <Grid style={{ marginTop: '4rem' }}>
        <Grid.Column width={15} style={{ background: 'white' }}>
          <Header as="h3">
            Sport
          </Header>
          <Container style={{ paddingLeft: '12px' }}>
            <Input
              placeholder="Category Name"
              type="text"
              onChange={(e) => this.setState({ categoryName: e.target.value })}
              value={categoryName}
            /> &nbsp; &nbsp; &nbsp;
            <Button
              className="login-button"
              onClick={() => { this.handleClick() }}
            >
              Add New Category
            </Button>

          </Container>
          <Grid columns={4} style={{ paddingLeft: '12px', marginTop: '10px' }}>
            <Grid.Column>
              <Menu compact>
                <Dropdown text='Select Category' options={options} simple item />
              </Menu>
            </Grid.Column>
            <Grid.Column>
              <Input
                placeholder="Sport Name"
                type="text"
                onChange={(e) => this.setState({ sportName: e.target.value })}
                value={sportName}
              />
            </Grid.Column>
            <Grid.Column>
              
            </Grid.Column>
            <Grid.Column>
            <Button
              className="login-button"
            >
              Browse icon
            </Button>
            </Grid.Column>
          </Grid>
          <Grid columns={12} style={{ display: 'flex', justifyContent: 'center'}}>
            <Button 
              className="login-button"
              onClick={() => this.addSport()}
            >
              Add New Sport
            </Button>
          </Grid>
          {
            options.map((option) => {
              return (
                <div>
                  <Header as='h5' dividing>
                    { 
                      option.text
                    }
                  </Header>
                  <Grid columns={12} key={option.key}>
                    <Menu icon="labeled" borderless>
                      {
                        option.sports.map((sport) => {
                          return (
                            <Menu.Item
                              name={sport.name}
                              key={sport.key}
                            >
                              <Icon name='video camera' />
                              {sport.name}
                            </Menu.Item>
                          )
                        })
                      }
                    </Menu>
                  </Grid>
                </div>
              )
            })
          }
        </Grid.Column>
      </Grid>
    )
  }
}

export default Sport;