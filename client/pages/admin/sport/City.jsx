import React, { Component } from 'react';
import {
  Menu,
  Header,
  Grid,
  Input,
  Button,
  Popup,
  Container
} from 'semantic-ui-react';

class City extends Component {
  constructor() {
    super();
    this.state = {
      activeItem: 'Lahore',
      menuItem: [{ cityName: 'Lahore' }],
      cityName: '',
      size: 'tiny',
      modalMessage: 'Are you sure you want to remove this City ?',
      open: false,
    }
  }

  show = size => () => this.setState({ size, open: true })
  close = () => this.setState({ open: false })

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  handleClick = () => {
    const { cityName, menuItem } = this.state;
    if (cityName) {
      menuItem.push({ cityName });
      this.setState({ cityName: '' })
    }
  }

  render() {
    const { activeItem, menuItem, cityName, open, size } = this.state;

    return (
      <Grid>
        <Grid.Column width={15} style={{ background: 'white' }}>
          <Header as="h3"> City </Header>
          <Container style={{ paddingLeft: '12px' }}>
            <Menu text>
              {
                menuItem.map((item) => {
                  return (
                    <Popup
                      basic
                      hoverable
                      trigger={
                        <Menu.Item
                          name={item.cityName}
                          active={activeItem === item.cityName}
                          onClick={this.handleItemClick}
                        />
                      }
                      content={
                        <div>
                          <span>
                            Edit
                          </span> |
                          <span onClick={this.show("tiny")} style={{ cursor: 'pointer' }}>
                            Remove
                          </span>
                        </div>
                      }
                    />
                  )
                })
              }
            </Menu>
            <Input
              placeholder="City Name"
              type="text"
              onChange={(e) => this.setState({ cityName: e.target.value })}
              value={cityName}
            /> &nbsp; &nbsp; &nbsp;
            <Button
              className="login-button"
              onClick={() => { this.handleClick() }}
            >
              Add New City
            </Button>
          </Container>
        </Grid.Column>
      </Grid>
    )
  }
}

export default City;