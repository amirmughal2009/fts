import React, { Component } from 'react';
import {
  Menu,
  Header,
  Grid,
  Icon,
  Button,
  Container,
  Dropdown
} from 'semantic-ui-react';

class JoinCity extends Component {
  constructor() {
    super();
    this.state = {
        selectedCitySports: [],
        cityName:'',
        cities: [
          {
              text: 'Jaipur',
              value: 1
          },
          {
              text: 'Mumbai',
              value: 2
          }
        ],
        categories: [
          {
            text: 'Fitness',
            value: 1
          },{
            text: 'Adventure Sports',
            value: 2
          },{
            text: 'All Sports',
            value: 3
          },
        ],
        sports:[
          {
            id:1,
            name: 'Cricket',
            icon: 'video camera'
          },
          {
            id:2,
            name: 'Football',
            icon: 'home'
          }
        ]
      }
  }

  handleClick = () => {
    const { cityName } = this.state;

    console.log(cityName);
  }

  render() {
    const { cities,categories,sports } = this.state;
    return (
      <Grid>
        <Grid.Column width={15} style={{ background: 'white' }}>
          <Header as="h3"> City </Header>
          <Container style={{ paddingLeft: '12px' }}>

            <Grid columns={2} style={{ paddingLeft: '12px', marginTop: '10px' }}>
              <Grid.Column>
                <p>Select City</p>
                <Menu compact>
                  <Dropdown
                    placeholder='Select City'
                    options={cities}
                    fluid selection
                    onChange={(e, {value}) => this.setState({ cityName: value })}
                    style={{ width: '15vw' }}
                    defaultValue={ cities && cities[0] && cities[0].value}
                  />
                </Menu>
              </Grid.Column>

              <Grid.Row>
                <Grid.Column width={4}>
                    <p>Select Category</p>
                    <Menu compact>
                    <Dropdown
                      placeholder='Select Category'
                      options={categories}
                      defaultValue={ categories && categories[0] && categories[0].value}
                      fluid selection
                      style={{ width: '15vw' }}
                    />
                    </Menu>  
                </Grid.Column>
                <Grid.Column width={12}  style={{ display:'flex', justifyContent:'left'}}>
                    <p>Select Sport</p>
                    <Menu icon="labeled" borderless>
                      {
                        sports.map((sport) => {
                          return (
                            <Menu.Item
                              name={sport.name}
                              key={sport.id}
                            >
                              <Icon size='tiny' name='video camera' />
                              {sport.name}
                            </Menu.Item>
                          )
                        })
                      }
                    </Menu>
                </Grid.Column>
              </Grid.Row>
            </Grid>
            <Grid columns={12} style={{ display:'flex', justifyContent:'center', marginBottom: '10px' }}>
              <Button
                className="login-button"
                onClick={() => { this.handleClick() }}>
                Join City
              </Button>
            </Grid>
          </Container>
        </Grid.Column>
      </Grid>
    )
  }
}

export default JoinCity;
