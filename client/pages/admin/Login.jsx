import React, { Component } from 'react';
import { Container, Header, Input, Button } from 'semantic-ui-react';
import { connect } from 'react-redux';
import  Actions  from '../../actions/AdminLoginActions';
import AppHOC from '../../hoc/AppHOC.jsx';

import './Login/login.less';

class Login extends Component {

  constructor() {
    super();
    
    this.state = {
      email: '',
      password: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
  }

  handleClick = () => {
    const { dispatch, client } = this.props;
    dispatch(Actions.AdminLoginAction(client, this.state));
  }

  handleChange = (key, e) => {
    this.setState({ [key]: e.target.value })
  }

  render() {
    return (
      <div>
        <Container fluid style={{ height: '9vh'}}>
          <span className="left-top-corner">Left</span>
          <span style={{ float: 'right' }} >
            <img src="../public/images/login/corner-image.png" className="corner-image" />
          </span>
        </Container>
        <Container fluid className="content-container">
          <img src="../public//images/logo/logo-login.png"
            className="logo-login"
          />
          <Container fluid position="right" className="form-content">
            <Header as="h1" textAlign="center" className="login-header" >Admin Login</Header><br/>
            <Input 
              fluid
              placeholder="Username or Email" 
              onChange={ (e) => {this.handleChange('email', e) }}
            />
            <br/>
            <Input
              fluid
              placeholder="Password"
              type="password"
              onChange={ (e) => { this.handleChange('password', e) }}
            /> 
            <br/><br/>
            <Button
              fluid 
              className="login-button"
              onClick={ () => {this.handleClick() }}
            >
              Login
            </Button>
          </Container>
        </Container>
        <Container fluid>
          <img src="../public/images/login/corner-image.png" className="left-bottom-image" />
        </Container>
      </div>
    )
  }
}

export default connect(
  state => ({
    adminLogin: state.adminLogin
  })
)(AppHOC(Login));
