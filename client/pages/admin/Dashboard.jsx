import React, { Component } from 'react';

import {
  Menu,
  Icon,
  Container,
  Grid,
  Header,
  Card,
  Statistic
} from 'semantic-ui-react';

class Dashboard extends Component {
  constructor() {
    super();
    this.state = {}
  }

  handleClick = (type) => {
    const { history } = this.props;
    history.push(`/${type}`);
  }

  render() {
    return (
      <div>
        <Header as="h2" disabled>
          Dashboard
        </Header>
        <Grid>
          <Grid.Column width={5}>
            <CardItem name={'Sport'} value={22} handleClick={() => this.handleClick('sport')}/>
          </Grid.Column>
          <Grid.Column width={5}>
            <CardItem name={'City'} value={12} handleClick={() => this.handleClick('city')}/>
          </Grid.Column>
          <Grid.Column width={5}>
            <CardItem name={'Quotes'} value={14} handleClick={() => this.handleClick('quotes')}/>
          </Grid.Column>
        </Grid>
      </div>
    )
  }
}

const CardItem = ({ name, value, handleClick }) => {
  return (
    <Card style={{ background: '#F05327' }} onClick={() => handleClick(name)}>
      <Card.Content>
        <Card.Header content={name} style={{ color: 'white' }} />
        <Statistic size='tiny' color='white' inverted style={{ float: 'right' }}>
          <Statistic.Value>{value}</Statistic.Value>
        </Statistic>
      </Card.Content>
    </Card>
  )
}

export default Dashboard;
