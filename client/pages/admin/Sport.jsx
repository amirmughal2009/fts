import React, { Component } from 'react';
import { Header } from 'semantic-ui-react';

import { RemoveModalBox } from '../../components/RemoveModal.jsx';
import City from './sport/City.jsx';
import SportCategory from './sport/Sport.jsx';

class Sport extends Component {
  constructor() {
    super();
    this.state = {
      size: 'tiny',
      modalMessage: 'Are you sure you want to remove this City ?',
    }
  }

  render() {
    const { activeItem, menuItem, cityName, open, size } = this.state;

    return (
      <div>
        <Header as="h2" disabled>
          Master
        </Header>
        <City />
        <SportCategory />
        <RemoveModalBox size={size} open={open} name="Are you sure you want to remove this City ?" close={() => this.close()} />
      </div>
    )
  }
}

export default Sport;