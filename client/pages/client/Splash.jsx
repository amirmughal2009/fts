import React, { Component } from 'react';
import { Container, Image } from 'semantic-ui-react';

class Splash extends Component {

  componentDidMount() {
    const { history } = this.props;
    setTimeout(() => {
      history.push('/home');
    }, 3000);
  }

  render() {
    return (
      <Container fluid style={{ height: '100vh', width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Image src='../public/images/logo/splash.jpeg' size='large' fluid style={{ height: '100%', width: '100%' }} />
      </Container>
    )
  }
}

export default Splash;
