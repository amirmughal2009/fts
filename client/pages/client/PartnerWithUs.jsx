import React, { Component } from 'react';
import { Container, Grid, Image, Form, TextArea, Button, Input } from 'semantic-ui-react';
import Header from '../../layouts/Header.jsx';
import SecondHeader from '../../layouts/SecondHeader.jsx';
import ClientFooter from '../../layouts/ClientFooter.jsx';
import Footer from './dashboard/Footer.jsx';
import './partner/partner.less';

class PartnerWithUs extends Component {
	render() {
	  return (
			<Container fluid style={{background: '#C4C4C4'}}>
				<Header isClientSide={true} />
				<SecondHeader />
				<Container textAlign='left' >
					<div 
						style={{
							textAlign: 'center', 
							marginTop: '77px', 
							paddingTop: '28px',
							textShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)', 
							color: '#F05327', 
							fontSize: '40px', 
							fontWeight: '500', 
							height: '500px', 
							backgroundImage: 'url("../public/images/partner-with-us/partner-with-us.jpg")', 
							backgroundRepeat: 'no-repeat' ,
							backgroundSize: 'cover' 
						}}>
					</div>
					<div className="backgroud">
						<div className="heading-text">
							Your Benefits as a FTS Partner
						</div>
						<br/><br/><br/><br/>
						<Grid className="primary-size">
							<Grid.Row columns={3}>
								<Grid.Column>
									<div className="center-align">
										<Image 
											className="top-step-image" 
											src='../public/images/partner-with-us/Vector(7).svg' />
									</div>
									<div className="simple-text">
									Active <span className="partner-color">Online Presence</span>
									</div>
								</Grid.Column>
								<Grid.Column>
									<div className="center-align">
										<Image 
											className="top-step-image" 
											src='../public/images/partner-with-us/Vector(8).svg' />
									</div>
									<div className="simple-text">
										<span className="partner-color">Marketing </span>
										and 
										<span className="partner-color"> Advertisement </span>
										of your arena
									</div>
								</Grid.Column>
								<Grid.Column>
									<div className="center-align">
										<Image 
											className="top-step-image" 
											src='../public/images/partner-with-us/Vector(6).svg' />
									</div>
									<div className="simple-text">
										<span className="partner-color">Increase </span>
										in
										<span className="partner-color"> customers </span>
										( Boost your sales)
									</div>
								</Grid.Column>
							</Grid.Row>

							<Grid.Row columns={3}>
								<Grid.Column>
									<div className="center-align">
										<Image 
											className="top-step-image" 
											src='../public/images/partner-with-us/Vector(2).svg' />
									</div>
									<div className="simple-text">
										<span className="partner-color">Ease of booking </span>
										for your customers
									</div> 
								</Grid.Column>
								<Grid.Column>
									<div className="center-align">
										<Image 
											className="top-step-image" 
											src='../public/images/partner-with-us/Vector(1).svg' />
									</div>
									<div className="simple-text">
										Players can now 
										<span className="partner-color"> book </span> for your arena 
										<span className="partner-color"> online </span>
									</div> 
								</Grid.Column>
								<Grid.Column>
									<div className="center-align">
										<Image 
											className="top-step-image" 
											src='../public/images/partner-with-us/Vector(5).svg' />
									</div>
									<div className="simple-text">
										<span className="partner-color">Payment </span> handling
									</div>
								</Grid.Column>
							</Grid.Row>

							<Grid.Row columns={2}>
								<Grid.Column>
									<div className="center-align">
										<Image 
											className="bottom-step-image" 
											src='../public/images/partner-with-us/Vector(3).svg' />
									</div>
									<div className="simple-text">
										<span className="partner-color">Infrastructure </span> and 
										<span className="partner-color"> resource </span> management
									</div>
								</Grid.Column>
								<Grid.Column>
									<div className="center-align">
										<Image 
											className="bottom-step-image" 
											src='../public/images/partner-with-us/Vector(4).svg' />
									</div>
									<div className="simple-text">
									<span className="partner-color">Database </span> of all your players or customers
									</div>
								</Grid.Column>
							</Grid.Row>
						</Grid>
						<br/><br/><br/>
						<div className="padding-bottom-20">
							<Image 
								className="image-style"
								src='../public/images/partner-with-us/underline.png'
							/>
						</div>
						<div className="heading-text">
							One stop to manage your complete sport arena
						</div>
						<div className="padding-bottom-20">
							<Image 
								className="image-style"
								src='../public/images/partner-with-us/underline.png'
							/>
						</div>
						<br/><br/><br/>
						<div className="heading-2-text">
							Steps to register with us
						</div>
						<br/><br/><br/>
						<Grid>
							<Grid.Row columns={3}>
								<Grid.Column>
									<div>
										<div className="heading-2-text">
											Step 1
										</div>
										<div className="center-align">
											<Image 
												className="partner-step-image" 
												src='../public/images/partner-with-us/step-1.png' />
											<Image 	
												className="partner-step-arrow-image" 
												src='../public/images/partner-with-us/Polygon.png' />
										</div>
									</div>
									<div className="center-align">
										<Button
											style={{marginTop: '10px'}}
											disabled
											className="login-button font-size-button center-align"
											>
											Fill the registration form bill and submit
										</Button>
									</div> 
								</Grid.Column>
								<Grid.Column>
									<div className="heading-2-text">
										Step 2
									</div>
									<div className="center-align">
										<Image 
											className="partner-step-image" 
											src='../public/images/partner-with-us/step-2.png' />
										<Image 	
											className="partner-step-arrow-image" 
											src='../public/images/partner-with-us/Polygon.png' />
									</div>
									<div className="center-align">
										<Button
											style={{marginTop: '10px'}}
											disabled
											className="login-button font-size-button center-align"
											>
											Verification and Approval by FTS team 
										</Button>
									</div>
								</Grid.Column>
								<Grid.Column>
									<div className="heading-2-text">
											Step 3
									</div>
									<div className="center-align">
										<Image 
											className="partner-step-image" 
											src='../public/images/partner-with-us/step-3.png' />
									</div>
									<div className="center-align">
										<Button
											style={{marginTop: '10px'}}
											disabled
											className="login-button font-size-button center-align"
											>
											You are on boarded
										</Button>
									</div>
								</Grid.Column>
							</Grid.Row>
						</Grid>
					</div>
					<div className="heading-2-text padding-bottom-20 padding-top-20">
						<b>REGISTRATION FORM</b>
					</div>
					<Form>
						<Form.Group className="center-align">
							<Form.Field 
								control={Input}
								placeholder='Name of Area'
								width={5}
							/>
							<Form.Input
								fluid
								placeholder='Enter your city'
								type="email"
								width={5}
							/>
							<Form.Input
								fluid
								placeholder='Please select your area'
								type="email"
								width={5}
							/>
						</Form.Group>
						<Form.Group className="center-align">
							<Form.Field 
								control={TextArea} 
								width={5}
								rows='8'
								placeholder='Address of area' 
							/>
							<Form.Field 
								control={TextArea} 
								width={10}
								rows='8'
								placeholder='Message' 
							/>
						</Form.Group>
						<Form.Group className="center-align padding-bottom-20">
							<Button
								className="login-button font-size-button"
								onClick={() => this.handleClick()}
								>
								Send
							</Button>
						</Form.Group>
					</Form>
				</Container>
				<ClientFooter />
				<Footer />
			</Container>
	  )
	}
}
  
export default PartnerWithUs;