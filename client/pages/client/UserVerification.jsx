import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Container, Grid, Header, Input, Button, Segment, Icon } from 'semantic-ui-react';
import './verify/verify.less'
import { connect } from 'react-redux';
import Actions from '../../actions/UserVerification';

class UserVerification extends Component{
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      phoneNo: '',
      eCode: '',
      pCode: ''
    }
  }

  handleChange = (key, e) => {
    this.setState({ [key]: e.target.value });
  }

  handleClick = (type) => {
    const { dispatch } = this.props;
    dispatch(Actions.UserVerificationAction({ ...this.state, type }));
  }

  render(){
    return (
      <Grid style={{ height:'100vh' }}>
        <Grid.Row columns={2}>

          <Grid.Column verticalAlign="middle">
            <Segment basic padded='very' style={{ width: '30vw', float: 'right' }}>
              <Header as='h1' className="header-verify-client">Verify Email Address</Header>
              <div>
                We just need your registerd email address to send verification code
              </div>
              <br/>
              <InputField 
                label='Email' 
                handleChange={this.handleChange} 
                inputKey='email' 
              />
              <InputField 
                label='Enter Code' 
                handleChange={this.handleChange} 
                inputKey='eCode' 
              />
              <Button 
                fluid 
                className="button-color" 
                onClick={() => this.handleClick('email')}
              >
                Verify Email Address
              </Button>
              <div className="grey-color">
                <label to="/" style={{ float:"right" }} >Resend Code</label>
              </div>
            </Segment>
          </Grid.Column>

          <Grid.Column verticalAlign="middle">
            <Segment basic padded='very' style={{ width: '30vw', float: 'left' }}>
              <Header as='h1' className="header-verify-client">Verify Phone Number</Header>
              <div>
                We just need your registerd phone number to send you verification code
              </div>
              <br/>
              <InputField 
                label='Phone' 
                handleChange={this.handleChange} 
                inputKey='phoneNo' 
              />
              <InputField 
                label='Enter Code' 
                handleChange={this.handleChange} 
                inputKey='pCode' 
              />
              <Button 
                fluid 
                className="button-color" 
                onClick={ () => this.handleClick('phone') }
              >
                Verify Phone Number
              </Button>
              <div className="grey-color">
                <label style={{ float:"right" }} to="/">Resend Code</label>
              </div>
            </Segment>
          </Grid.Column>

        </Grid.Row>
        <div className="primary-color" style={{ width:"100vw", fontSize:"20px" }}>
          <label style={{ float:"right" }} to="/">
            Verification Completed
            <Icon name="arrow right">
            </Icon>
          </label>
        </div>
      </Grid>
    )
  }
}

export default connect(
  state => ({
    user: state
  })
)(UserVerification);

const InputField = ({ handleChange, inputKey, label }) => {
  return (
    <div>
      <label className="disabled-label">{label}</label>
      <br/>
      <Input 
        transparent 
        fluid 
        className="disabled-input margin-10"
        onChange={ (e) => handleChange(inputKey, e) } 
      />  
    </div>
  ) 
}