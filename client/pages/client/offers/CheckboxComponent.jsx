import React, { Component } from 'react';
import { Checkbox, Grid } from 'semantic-ui-react'

const CheckboxComponent = ({ name }) => {
	console.log(name);
	return (
		<Grid.Row>
			<Grid.Column>
				<Checkbox className='catagory' style={{fontSize: '24px', margin: '12px 0 0 21px'}} label={{ children: name }} />
			</Grid.Column>
		</Grid.Row>
	)
}

export default CheckboxComponent;