import React, { Component } from 'react';
import { Container, Grid, Header, Image, Button, List } from 'semantic-ui-react';

class ViewDetails extends Component {
	render() {
		return (
			<Container>
				<Grid style={{margin: 0, padding: '40px 52px 0 28px'}}>
					<Grid.Row>
						<Grid.Column width={6}>
							<div style={{padding: '20px 53px', marginRight: '35px', border: '4px solid #F05327'}}>
								<Image src='../public/images/dashboard/offers.jpeg' wrapped />
							</div>
							<Header as='h1' className='item-text'>20% Off on hockey</Header>
							<Button className='cp-code-button' positive>Cupon Code : FTS200</Button>
						</Grid.Column>
						<Grid.Column width={10}>
							<Grid style={{textAlign: 'center', marginTop: '0'}}>
								<ItemInfo catogory={'CITY'} name={'Mumbai'} grid_width={5} />
								<ItemInfo catogory={'CATEGORY'} name={'Fitness'} grid_width={6} />
								<ItemInfo catogory={'SPORT'} name={'Football'} grid_width={5} />
								<ItemInfo catogory={'AREA '} name={'Sadar'} grid_width={5} />
								<ItemInfo catogory={'ARENA '} name={'Sports Club'} grid_width={6} />
								<ItemInfo catogory={'DISCOUNT '} name={'30%'} grid_width={5} />
								<ItemInfo catogory={'VALIDITY '} name={'17/10/2018'} grid_width={6} />
								<ItemInfo catogory={'APPLICABLE ON '} name={'All'} grid_width={10} />
							</Grid>
						</Grid.Column>
						<div style={{fontSize: '19px'}}>
							<Header as='h3' className='item-text1' style={{margin: '50px 0 0 16px'}}>Description</Header>
							<List bulleted>
    						<List.Item>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
								</List.Item>
								<List.Item>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
								</List.Item>
								<List.Item>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
								</List.Item>
								<List.Item>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
								</List.Item>
								<List.Item>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
								</List.Item>
							</List>
							<Header as='h3' className='item-text1' style={{margin: '50px 0 0 16px'}}>Terms and Conditions</Header>
							<List bulleted>
    						<List.Item>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
								</List.Item>
								<List.Item>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
								</List.Item>
								<List.Item>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
								</List.Item>
								<List.Item>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
								</List.Item>
								<List.Item>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
								</List.Item>
							</List>
						</div>
					</Grid.Row>
				</Grid>
			</Container>
		)
	}
}

export default ViewDetails;

const ItemInfo = ({ catogory, name, grid_width }) => {
	return (
		<Grid.Column width={grid_width} style={{padding: '5px'}}>
			<div style={{padding: '10px', boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)', border: '1px solid #E1DDDD'}}>
				<Header as='h2' style={{margin: 0}}>{catogory}</Header>
				<Header as='h2' style={{color: '#F05327',margin: 0}}>{name}</Header>
			</div>
		</Grid.Column>
	)
}