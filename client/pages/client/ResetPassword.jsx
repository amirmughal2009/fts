import React, { Component } from 'react';
import { Grid, Icon, Header, Form, Button, Input, Message } from 'semantic-ui-react';
import './reset-password/style.less';
import { connect } from 'react-redux';
import Actions from '../../actions/ResetPasswordActions';
import AppHOC from '../../hoc/AppHOC.jsx';

class ResetPassword extends Component {
  constructor(){
    super();
    this.state = {
      password: '',
      phoneNo:'03238371033',
      retypePassword: '',
      err: false,
      message: ''
    };
  }

  handleClick = () => {
    if (  !this.state.password || 
          !this.state.retypePassword
    ){
      this.setState({ 
        err: true, 
        message: 'Please enter correct password'
      });
    } else if ((this.state.password != this.state.retypePassword)){
      this.setState({ 
        err: true, 
        message: 'Password mismatched'
      });
    } else if(this.state.password.length < 6){
      this.setState({ 
        err: true, 
        message: 'Please enter atleast 6 characters'
      });
    } else {
      const { dispatch, client } = this.props;
      dispatch(Actions.resetPasswordAction(client, this.state));
    }
  }

  handleChange = (key, e) => {
    this.setState({ 
      err: false, 
      [key]: e.target.value
    });
  }

  componentWillReceiveProps = (nextProps) => {
    const { resetPassword } = nextProps;
    const { status, msg, data } = resetPassword;
    if(status) {
      const { history } = this.props;
      history.push('/login');
    }else{
      this.setState({
        err: true,
        message: msg
      });
    }
  }

  render() {
    return(
      <Grid textAlign="center" style={{ height: '100vh' }}>
        <Grid.Column verticalAlign="middle">
          { this.state.err && 
            <Message
              error
              style={{ width: '24%', marginLeft:'40%' }}
              header='Reset Password Error'
              content= { this.state.message }
            />
          }
          <Form>
            <Icon name='lock' size='huge' className="icon-color" />
            <Header as='h1' className="header-login-client">Reset Password ?</Header>
            <br />
            <Form.Field>
            <label className="disabled-label">New Password</label>
            <Input transparent
              type="password" 
              className="disabled-input margin-10" 
              style={{ width: '24%' }}
              onChange={(e) => this.handleChange('password', e)}
            />
            </Form.Field>
            <Form.Field>
            <label className="disabled-label">Retype Password</label>
            <Input transparent 
              type="password"
              className="disabled-input margin-10" 
              style={{ width: '24%' }}
              onChange={(e) => this.handleChange('retypePassword', e)}
            />
            </Form.Field>
            <br />
            <Form.Field>
              <Button
                onClick={() => this.handleClick()}
                className="login-button font-size-button"
              >
                Reset Password
              </Button>
            </Form.Field>
          </Form>
        </Grid.Column>
      </Grid>
    )
  }
}

export default connect(
  state => ({
    resetPassword: state.resetPassword
  })
)(AppHOC(ResetPassword));

