import React, { Component } from 'react';
import Header1 from '../../layouts/Header.jsx';
import SecondHeader from '../../layouts/SecondHeader.jsx';
import ClientFooter from '../../layouts/ClientFooter.jsx';
import Footer from './dashboard/Footer.jsx';
import ArenaDetails from './booknplaydetails/ArenaDetails.jsx'
import RateTiming from './booknplaydetails/RateTiming.jsx'
import ArenaTabs from './booknplaydetails/ArenaTabs.jsx'
import { Container, Grid } from 'semantic-ui-react';

class BooknPlayDetails extends Component {
    render() {
      return (
        <Container fluid style={{background: '#EBE8E8'}}>
          <Header1 isClientSide={true} />
          <SecondHeader />
         
          <ArenaDetails />
          <RateTiming />
          <ArenaTabs />  
          <ClientFooter />
          <Footer />
        </Container>
      )
    }
  }
  
  export default BooknPlayDetails;