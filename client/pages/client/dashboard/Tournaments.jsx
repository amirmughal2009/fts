import React, { Component } from 'react';
import { Container, Button, Image, Grid, Card, Icon } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

class Tournaments extends Component {
  render() {
    return (
      <Container fluid style={{background: '#000', padding: '40px 60px'}}>
        <h1 style={{color: '#FC9B04'}}>Tournaments</h1>
        <Grid columns={4} style={{}}>
						<Grid.Row>
							<TournamentItem Imagename={'../public/images/dashboard/qatarsports-footballqatar-620x4001.png'} />
							<TournamentItem Imagename={'../public/images/dashboard/TennisBallOnCourt.png'} />
							<TournamentItem Imagename={'../public/images/dashboard/Badminton Tournaments.png'} />						
						<Grid.Column verticalAlign='middle'>
							<div style={{position: 'relative'}}>
								<Image src='../public/images/logo/Rectangle1.png' style={{ width: '150px', height: '45px', margin: '15px auto'}} />            
								<span className='offer-text' style={{color: '#FFFFFF', fontWeight: 'bold', fontSize: '20px', top: '15px', margin: '0px 0 0 100px'}}>View All</span>
							</div>
						</Grid.Column>
					</Grid.Row>
				</Grid>
      </Container>
    )
  }
}

export default Tournaments;

const TournamentItem = ({ Imagename }) => {
	return (
		<Grid.Column>
			<Card style={{borderRadius: '35px 35px 0px 0px', border: '5px solid #FFFFFF'}}>
				<Image style={{height: '250px'}} src={Imagename} />
				<Card.Content style={{background: '#19191B', paddingTop: '30px'}}>
					<Card.Header className='tournament-text' style={{color: '#FFFFFF', textAlign: 'center'}}>NAME OF TORNAMENT</Card.Header>
					<div style={{paddingTop: '20px'}}>
						<div style={{width: '50%', float: 'left', marginTop: '5px'}}>
							<Image src='../public/images/dashboard/location.png' avatar />
							<span style={{color: '#FC9B04'}}>Location</span>
						</div>
						<div style={{position: 'relative', width: '50%', float: 'right'}}>
							<Image src='../public/images/logo/Rectangle1.png' style={{ width: '90px', height: '30px', float: 'right'}} />            
							<span className='offer-text details-btn' style={{ color: '#FFFFFF', fontWeight: 'bold', fontSize: '14px', top: '7px', right: '20px'}}>Details</span>
						</div>
					</div>
				</Card.Content>
			</Card>
		</Grid.Column>
	)
}