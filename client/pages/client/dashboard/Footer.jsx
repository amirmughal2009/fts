import React, { Component } from 'react';
import { Container } from 'semantic-ui-react';

class Footer extends Component {
  render() {
    return (
      <Container fluid textAlign='center' style={{ height: '80px', paddingTop: '20px' ,fontSize: '18px', backgroundColor: '#FFFFFF' }}>
        © 2018 ftsgroup All rights reserved
      </Container>
    )
  }
}

export default Footer;
