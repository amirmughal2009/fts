import React, { Component } from 'react';
import { Carousel } from 'react-responsive-carousel';
import { Grid, Button, Icon, Image, Label } from 'semantic-ui-react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import "react-responsive-carousel/lib/styles/examples/presentation/presentation.min.css";

class Slider extends Component {
  render() {
    return (
      <Carousel
        showThumbs={false}
        showStatus={false}
        useKeyboardArrows
        className=""
      >
        <div className="my-slide1" style={{height: '423px', background: 'linear-gradient(99.8deg, #EF9322 40.7%, #FFFFFF 96.47%)'}}>
        <Grid padded='horizontally' textAlign='left' style={{padding: '0px 0px'}}>
          <Grid.Column width={5} style={{paddingLeft: 0}}>
            <Image wrapped style={{margin: '30px'}} src='../public/images/dashboard/banner2img.png' />
            {/* <Image src='../public/images/dashboard/dash-Polygon2.png' style={{width: '120px', height: '211px'}} /> */}
          </Grid.Column>
          <Grid.Column width={6} floated='left' style={{color: '#ffffff', paddingLeft: '50px'}}>
            <h1 style={{marginTop: '100px', fontSize: '40px', fontFamily: "'Ruda', sans-serif"}}>
              FEEL THE SPORT
            </h1>
            <p style={{margin: '35px 0', fontSize: '18px', fontFamily: "'Rufina', serif"}}>
              It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. 
            </p>
            <Image src='../public/images/dashboard/leran-rec.png' style={{float: 'left', width: ' 260px', height: '47px'}} />            
            <span className='offer-text' style={{ color: '#FFFFFF', left: '80px', fontWeight: 'bold', fontSize: '24px', top: '326px'}}>BOOK N PLAY</span>
            
          </Grid.Column>
          <Grid.Column width={5} style={{paddingRight: 0, background: 'url("../public/images/dashboard/slider-rec.png") no-repeat'}}>
            <Grid textAlign='center' style={{height: 'inherit', color: '#19191B'}}>
            <Grid.Row style={{paddingLeft: '116px'}}>
              <p style={{color: '#F05327', fontSize: '20px', float: 'left', width: '100%', textAlign: 'center', margin: 0}}>
                Live Score
              </p>
              <h1 style={{margin: 0, fontSize: '25px'}}>
                Name of Tournament
              </h1>
              <p style={{float: 'left', width: '100%', textAlign: 'center', margin: 0}}>Team 1 Vs Team2</p>
            </Grid.Row>
            <Grid.Row style={{margin: '10px 0 10px 62px', color: '#fff',background: 'url("../public/images/dashboard/Rectangle4.png") no-repeat'}}>
              <Grid.Column width={8}>
                <h3 style={{color: '#000'}}>Team 1</h3>
                <p style={{margin: '35px 0 0 0', color: '#C7C7C7'}}>Runs</p>
                <h3 style={{margin: 0}}>50 / 102</h3>
              </Grid.Column>
              <Grid.Column width={8}>
                <h3>Run rate : 3</h3>
                <p style={{margin: '35px 0 0 0', color: '#C7C7C7'}}>Wickets</p>
                <h3 style={{margin: 0}}>01</h3>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row style={{marginLeft: '15px', color: '#fff', background: 'url("../public/images/dashboard/Rectangle4.1.png") no-repeat'}}>
              <Grid.Column width={8}>
                <h3 style={{color: '#000'}}>Team 1</h3>
                <p style={{margin: '35px 0 0 0', color: '#C7C7C7'}}>Runs</p>
                <h3 style={{margin: 0}}>50 / 102</h3>
              </Grid.Column>
              <Grid.Column width={8}>
                <h3>Run rate : 3</h3>
                <p style={{margin: '35px 0 0 0', color: '#C7C7C7'}}>Wickets</p>
                <h3 style={{margin: 0}}>01</h3>
              </Grid.Column>
            </Grid.Row>
            {/* <Image src='../public/images/dashboard/Polygon.png' style={{width: '120px', height: '240px', float: 'right', position: 'absolute', top: '-80px', right: 0}} /> */}
            {/* <Image src='../public/images/dashboard/dash-Polygon1.png' style={{width: '120px', height: '550px', float: 'right'}} /> */}
            </Grid>
          </Grid.Column>
        </Grid>
        </div>
        <div className="my-slide" style={{background: '#1b1c1d'}}>
          <h2>
            It's just a couple of new styles...
          </h2>
        </div>
      </Carousel>
    )
  }
}

export default Slider;
