import React, { Component } from 'react';
import { Grid, Image, Button } from 'semantic-ui-react';

class WinMatches extends Component {
  render() {
    return (
      <Grid style={{ backgroundColor: '#19191B', height: '40vh', display: 'flex', justifyContent: 'center' }}>
        <Grid.Column width={2} verticalAlign='middle'>
          <Image
            src='../public/images/home/win.png'
            wrapped
          />
        </Grid.Column>
        <Grid.Column width={4}  verticalAlign='middle'>
          <div>
            <div className='win-text' style={{ color: 'white', lineHeight: '44px', fontSize: '36px' }}>
              Win matches &
            </div>
            <div className='win-text' style={{ color: '#EF9322', fontSize: '36px', lineHeight: '44px' }}> 
              win reward points
            </div> 
          </div>
        </Grid.Column>
        <Grid.Column width={3} verticalAlign='middle'>
          <Image src='../public/images/dashboard/win-btn-rec.png' style={{ width: '171px', height: '45px', margin: '0 auto'}} />            
          <span className='offer-text' style={{fontFamily: "'Rufina', serif", color: '#EF9322', fontSize: '20px', top: '28px', fontWeight: 'bold', margin: '0px 0 0 59px'}}>Know More</span>
        </Grid.Column>
      </Grid>
    )
  }
}


export default WinMatches;