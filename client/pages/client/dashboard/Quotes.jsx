import React, { Component } from 'react';
import { Container } from 'semantic-ui-react';

class Quotes extends Component {
  render() {
    return (
      <Container fluid textAlign="center" style={{ height: '12vh', fontSize: '26px', fontFamily: "'Ruda', sans-serif", justifyContent: 'center', display: 'flex', alignItems: 'flex-end', background: '#E1DDDD' }}>
        <b>“Winning isn't everything, it's the only thing.”</b>
      </Container>
    )
  }
}

export default Quotes;
