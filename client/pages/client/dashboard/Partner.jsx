import React, { Component } from 'react';
import { Grid, Container, Header, Button, Image } from 'semantic-ui-react';

class Partner extends Component {
  render() {
    return (
      <Container fluid>
        <Grid style={{height: '70vh' ,background: "url('../public/images/dashboard/partner-back1.png') no-repeat" ,backgroundColor: 'rgb(25, 25, 27)', padding: '50px 110px' }}>
          <Grid.Column width={8} style={{padding: '100px'}}>
            <Image
              src='../public/images/home/partner.png'
            />
          </Grid.Column>
          <Grid.Column width={8} style={{padding: '55px'}}>
            <Header  as='h1' style={{ fontSize: '30px' }}>Why Partner With Us ?</Header>
            <div>
              There are many variations of passages of Lorem Ipsum available,  but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary. There are many variations of passages of Lorem Ipsum available,  but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you 
            </div>
            <br />
            <div style={{position: 'relative'}}>
              <Image src='../public/images/dashboard/part-btn-rec.png' style={{ width: '320px', height: '60px'}} />            
              <a
                href="/partner-with-us" 
                className='offer-text' 
                style={{
                  fontFamily: "'Rufina', serif",
                  color: '#19191B',
                  fontWeight: 'bold', 
                  fontSize: '26px', top: '20px', 
                  margin: '0px 0 0 64px'
                  }}>
                  Partner With Us
                </a>
            </div>
          </Grid.Column>
        </Grid>
      </Container>
    )
  }
}

export default Partner;
