import React, { Component } from 'react';
import { Container, Image, Grid, Button } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

class Offers extends Component {
  render() {
    return (
      <Container fluid textAlign='center' style={{background: "url('../public/images/dashboard/offers-back1.png') no-repeat" ,backgroundColor: '#000' }}>
        <Grid style={{ display: 'flex', paddingLeft: '130px', paddingBottom: '50px' }}>
          <Grid.Row>
            <h1 style={{fontSize: '30px', margin: '50px 0 0 38px', fontWeight: '400'}}>Best Offers</h1>
          </Grid.Row>
          <Grid.Column width={4}>
            <div>
              <Image 
                src='../public/images/dashboard/offers.jpeg'
                style={{ padding: '25px', height: '230px', paddingBottom: '3px' }}
                fluid
              />
              <div style={{ color: 'white', fontSize: '25px', lineHeight: '42px' }} >
                30% off on Women Hockey
              </div>
            </div>
          </Grid.Column>
          <Grid.Column width={4}>
            <div>
              <Image 
                src='../public/images/dashboard/offers-img1.png'
                style={{ padding: '25px', height: '230px' , paddingBottom: '3px' }}
                fluid
              />
              <div style={{ color: 'white', fontSize: '25px', lineHeight: '42px' }} >
                30% off on Women Hockey
              </div>
            </div>
          </Grid.Column>
          <Grid.Column width={4}>
            <div>
              <Image 
                src='../public/images/dashboard/offers-img2.png'
                style={{ padding: '25px', height: '230px' , paddingBottom: '3px' }}
                fluid
              />
              <div style={{ color: 'white', fontSize: '25px', lineHeight: '42px' }} >
                30% off on Women Hockey
              </div>
            </div>
          </Grid.Column>
          <Grid.Column width={3} verticalAlign='middle'>
            <Image src='../public/images/logo/Rectangle1.png' style={{ width: '140px', height: '45px', margin: '0 auto'}} />            
            <span className='offer-text' style={{fontFamily: "'Rufina', serif", color: '#FFFFFF', fontWeight: 'bold', fontSize: '20px', top: '28px', margin: '0px 0 0 -41px'}}>View All</span>
          </Grid.Column>
        </Grid>     
      </Container>
    )
  }
}

export default Offers;
