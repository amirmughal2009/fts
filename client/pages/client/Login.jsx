import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Container, Grid, Image, Header, Input, Button, Icon, Segment, Message } from 'semantic-ui-react';
import './login/login.less';
import { connect } from 'react-redux';
import Actions from '../../actions/UserLoginActions';
import AppHOC from '../../hoc/AppHOC.jsx';

class Login extends Component {
  constructor() {
    super();
    this.state = {
      phoneNo: '',
      password: '',
      phoneErr: false,
      passwordErr: false,
      err:false,
      message:''
    }
  }

  handleClick = () => {
    if (!this.state.phoneNo){
      this.setState({ 
        err: true, 
        message: 'Please enter phone number',
        phoneErr: true 
      });
    } else if (!this.state.password){
      this.setState({ 
        err: true, 
        message: 'Please enter password',
        passwordErr: true
      });
    } else {
      const { dispatch, client } = this.props;
      dispatch(Actions.UserLoginAction(client, this.state));
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const { userLogin } = nextProps;
    const { status, msg, data } = userLogin;
    if(status) {
      const { history } = this.props;
      history.push('/home');
    }else{
      this.setState({
        err: true,
        message: msg
      });
    }
  }

  handleChange = (key, e) => {
    this.setState({ 
      phoneErr: false, 
      err: false, 
      passwordErr: false, 
      [key]: e.target.value
    });
  }

  render() {
    return (
      <Container fluid style={{ height: '100vh' }}>
        <Grid divided='vertically'>
          <Grid.Row columns={2}>
            <Grid.Column verticalAlign="middle">
              <Segment basic padded='very' style={{ width: '37vw', float: 'right' }}>
                <Header as='h1' className="header-login-client">Login</Header>
                <div>
                  Don’t have an account? <Link to="/signup">Create an account</Link>
                </div>
                { this.state.err && 
                    <Message
                      error
                      header='Login Error'
                      content= { this.state.message }
                    />
                }
                <br />
                <Input
                  fluid
                  error={this.state.phoneErr}
                  placeholder="Phone Number"
                  onChange={(e) => this.handleChange('phoneNo', e)}
                />
                <br />
                <Input
                  fluid
                  error={this.state.passwordErr}
                  placeholder="Password"
                  type="password"
                  onChange={(e) => this.handleChange('password', e)}
                />
                <br />
                <Button
                  fluid
                  className="login-button font-size-button"
                  onClick={() => this.handleClick()}
                >
                  Login
                </Button>
                <div style={{ float: 'right' }}> <Link to="/forgot-password">Forgot password ?</Link></div>
                <br />
                <div style={{ textAlign: 'center' }}>OR</div>
                <br />
                <Button color='facebook' fluid className="font-size-button">
                  <Icon name='facebook f' style={{ float: 'left' }} /> Login with facebook
                </Button>
                <br />
                <Button color='google plus' fluid className="font-size-button">
                  <Icon name='mail' style={{ float: 'left' }} /> Login with gmail
                </Button>
              </Segment>
            </Grid.Column>
            <Grid.Column>
              <div>
                <Image
                  className="login-corner-image" 
                  src='../public/images/authentication/login-corner.png' 
                  fluid 
                />
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    )
  }
}

export default connect(
  state => ({
    userLogin: state.userLogin
  })
)(AppHOC(Login));
