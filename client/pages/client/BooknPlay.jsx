import React, { Component } from 'react';
import { Card, Icon, Image, Container, Grid, Segment, Checkbox, Rating, Button } from 'semantic-ui-react'
import Header from '../../layouts/Header.jsx';
import SecondHeader from '../../layouts/SecondHeader.jsx';
import ClientFooter from '../../layouts/ClientFooter.jsx';
import Sidebar from './booknplay/Sidebar.jsx';
import './offers/offers.less';

class BooknPlay extends Component {
	render() {
		return (
			<Container fluid>
				<Header isClientSide={true} />
				<SecondHeader />

				<Grid className='offer-container'>
					<Grid.Row>
						<Grid.Column width={5}>
							<Sidebar />
						</Grid.Column>
						<Grid.Column width={10}>
							<h1 style={{ color: '#F05327', margin: '35px 0 27px 0' }}>BOOK N PLAY</h1>
							<Card.Group>
								<Card fluid className="soccer-card" style={{ height: '490px', backgroundImage: 'url("../public/images/booknplay/soccer.jpg")', backgroundRepeat: 'no-repeat', backgroundSize: 'cover' }}>
									<Card.Content>
										<Card.Header style={{ float: 'left', width: '100%', marginTop: '10px' }}>
											<div style={{ float: 'left' }}>
												<p style={{ color: '#FC9B04', margin: 0 }}>4.5</p>
												<Rating defaultRating={4} maxRating={5} />
											</div>
											<div style={{ float: 'right', color: '#FFFFFF' }}>
												Price : Rs 100 / hour
												</div>
										</Card.Header>
										<div style={{ fontSize: '30px', float: 'left', width: '100%', textAlign: 'center', marginTop: '14%', color: '#FFFFFF', height: '230px' }}>
											<p style={{ fontSize: '36px', margin: 0 }}>NAME OF ARENA</p>
											<image src='../public/images/booknplay/if_simpline_8_2305589.png'></image>
											<p>Area of arena</p>
											<Button className="details-button" >
												Details
												</Button>
										</div>
										<div style={{ width: '100%' }}>
											<div style={{ float: 'left', marginTop: '25px' }}>
												<image src='../public/images/booknplay/Vector.png'></image>
												<p style={{ fontSize: '24px', color: '#FFFFFF' }}>Get Directions</p>
											</div>
											<div style={{ float: 'right' }}>
												<Button className="membership-button" >
													Membership
													</Button>
												<Button className="booknow-button" >
													Book Now
													</Button>
											</div>
										</div>
									</Card.Content>
								</Card>
								<Card fluid className="soccer-card" style={{ height: '490px', backgroundImage: 'url("../public/images/booknplay/soccer.jpg")', backgroundRepeat: 'no-repeat', backgroundSize: 'cover' }}>
									<Card.Content>
										<Card.Header style={{ float: 'left', width: '100%', marginTop: '10px' }}>
											<div style={{ float: 'left' }}>
												<p style={{ color: '#FC9B04', margin: 0 }}>4.5</p>
												<Rating defaultRating={4} maxRating={5} />
											</div>
											<div style={{ float: 'right', color: '#FFFFFF' }}>
												Price : Rs 100 / hour
												</div>
										</Card.Header>
										<div style={{ fontSize: '30px', float: 'left', width: '100%', textAlign: 'center', marginTop: '14%', color: '#FFFFFF', height: '230px' }}>
											<p style={{ fontSize: '36px', margin: 0 }}>NAME OF ARENA</p>
											<image src='../public/images/booknplay/if_simpline_8_2305589.png'></image>
											<p>Area of arena</p>
											<Button className="details-button" >
												Details
												</Button>
										</div>
										<div style={{ width: '100%' }}>
											<div style={{ float: 'left', marginTop: '25px' }}>
												<image src='../public/images/booknplay/Vector.png'></image>
												<p style={{ fontSize: '24px', color: '#FFFFFF' }}>Get Directions</p>
											</div>
											<div style={{ float: 'right' }}>
												<Button className="membership-button" >
													Membership
													</Button>
												<Button className="booknow-button" >
													Book Now
													</Button>
											</div>
										</div>
									</Card.Content>
								</Card>
								<Card fluid className="soccer-card" style={{ height: '490px', backgroundImage: 'url("../public/images/booknplay/soccer.jpg")', backgroundRepeat: 'no-repeat', backgroundSize: 'cover' }}>
									<Card.Content>
										<Card.Header style={{ float: 'left', width: '100%', marginTop: '10px' }}>
											<div style={{ float: 'left' }}>
												<p style={{ color: '#FC9B04', margin: 0 }}>4.5</p>
												<Rating defaultRating={4} maxRating={5} />
											</div>
											<div style={{ float: 'right', color: '#FFFFFF' }}>
												Price : Rs 100 / hour
												</div>
										</Card.Header>
										<div style={{ fontSize: '30px', float: 'left', width: '100%', textAlign: 'center', marginTop: '14%', color: '#FFFFFF', height: '230px' }}>
											<p style={{ fontSize: '36px', margin: 0 }}>NAME OF ARENA</p>
											<image src='../public/images/booknplay/if_simpline_8_2305589.png'></image>
											<p>Area of arena</p>
											<Button className="details-button" >
												Details
												</Button>
										</div>
										<div style={{ width: '100%' }}>
											<div style={{ float: 'left', marginTop: '25px' }}>
												<image src='../public/images/booknplay/Vector.png'></image>
												<p style={{ fontSize: '24px', color: '#FFFFFF' }}>Get Directions</p>
											</div>
											<div style={{ float: 'right' }}>
												<Button className="membership-button" >
													Membership
													</Button>
												<Button className="booknow-button" >
													Book Now
													</Button>
											</div>
										</div>
									</Card.Content>
								</Card>
							</Card.Group>
						</Grid.Column>
					</Grid.Row>
				</Grid>
				<ClientFooter />
			</Container>
		)
	}
}

export default BooknPlay;