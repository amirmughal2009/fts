import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Grid, Image, Form, Checkbox, Button, Header, Message } from 'semantic-ui-react';
import { connect } from 'react-redux';
import Actions from '../../actions/UserRegisterActions';
import AppHOC from '../../hoc/AppHOC.jsx';
import './signup/signup.less';
import validator from 'validator';

const options = [
  { key: 1, text: 'Male', value: 'male' },
  { key: 2, text: 'Female', value: 'female' },
  { key: 3, text: 'Other', value: 'other' },
]

class Signup extends Component {
  constructor() {

    super();

    this.state = {
      fullName: '',
      email: '',
      password: '',
      confirmPassword: '',
      gender: '',
      dateOfBirth: '',
      phoneNo: '',
      nameErr: false,
      emailErr: false,
      passwordErr: false,
      confirmPasswordErr: false,
      genderErr: false,
      dateOfBirthErr: false,
      phoneNoErr: false,
      err: false,
      message: ''
    }
  }

  handleClick = () => {
    if (!this.state.fullName) {
      this.setState({
        err: true,
        message: 'Please enter full name',
        nameErr: true
      });
    } else if (!this.state.email || !validator.isEmail(this.state.email)) {
      this.setState({
        err: true,
        message: 'Please enter correct email address',
        emailErr: true
      });
    } else if (!this.state.phoneNo) {
      this.setState({
        err: true,
        message: 'Please enter correct phone number',
        phoneNoErr: true
      });
    } else if (!this.state.dateOfBirth) {
      this.setState({
        err: true,
        message: 'Please enter date',
        dateOfBirthErr: true
      });
    } else if (!this.state.password) {
      this.setState({
        err: true,
        message: 'Please enter password',
        passwordErr: true
      });
    } else if (!this.state.confirmPassword) {
      this.setState({
        err: true,
        message: 'Please re-enter password',
        confirmPasswordErr: true
      });
    } else if (this.state.password != this.state.confirmPassword) {
      this.setState({
        err: true,
        message: 'Password mismatch. Please enter correct password',
      });
    } else {
      const { dispatch, client } = this.props;
      dispatch(Actions.UserRegisterAction(client, this.state));
    }
  }

  handleChange = (key, e) => {
    this.setState({
      err: false,
      nameErr: false,
      emailErr: false,
      phoneNoErr: false,
      dateOfBirthErr: false,
      passwordErr: false,
      confirmPasswordErr: false,
      passwordMismatchErr: false,
      genderErr: false,
      [key]: e.target.value
    });
  }

  componentWillReceiveProps = (nextProps) => {
    const { user } = nextProps;
    const { status, msg, data } = user;
    if(status) {
      const { history } = this.props;
      history.push('/verify-account');
    }else{
      this.setState({
        err: true,
        message: msg
      });
    }
  }

  render() {
    return (
      <Grid columns='equal'>
        <Grid.Column width={5}>
          <Image
            className="login-corner-image"
            src='../public/images/authentication/signup.png'
            fluid
          />
        </Grid.Column>
        <Grid.Column width={10} verticalAlign="middle">
          <Header as='h1' className="header-login-client">Sign Up</Header>
          {this.state.err &&
            <Message
              error
              header='Signup Error'
              content={this.state.message}
            />
          }
          <Form>
            <Form.Group>
              <Form.Input
                fluid
                error={this.state.nameErr}
                label='Name'
                placeholder='Name'
                width={8}
                onChange={(e) => this.handleChange('fullName', e)}
              />
              <Form.Input
                fluid
                label='Email'
                placeholder='Email'
                type="email"
                width={8}
                error={this.state.emailErr}
                onChange={(e) => this.handleChange('email', e)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Input
                fluid
                label='Phone no'
                placeholder='Phone no'
                width={8}
                error={this.state.phoneNoErr}
                onChange={(e) => this.handleChange('phoneNo', e)}
              />
              <Form.Input
                fluid
                label='Date of birth'
                placeholder='Date of birth'
                width={8}
                error={this.state.dateOfBirthErr}
                onChange={(e) => this.handleChange('dateOfBirth', e)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Input
                fluid
                label='Password'
                placeholder='Password'
                type="password"
                width={8}
                error={this.state.passwordErr}
                onChange={(e) => this.handleChange('password', e)}
              />
              <Form.Dropdown
                search
                selection
                className="signup-dropdown"
                label="Select Gender"
                wrapSelection={false}
                options={options}
                placeholder='Choose gender'
                width={8}
                error={this.state.genderErr}
                onChange={(e, { value }) => this.setState({ 'gender': value })}
              />
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.Input
                fluid
                label='Retype Password'
                placeholder='Retype Password'
                type="password"
                width={8}
                error={this.state.confirmPasswordErr}
                onChange={(e) => this.handleChange('confirmPassword', e)}
              />
            </Form.Group>
            <Form.Group>
              <Checkbox label='By signing up I agree with Terms and Conditions' />
            </Form.Group>
            <Form.Field inline>
              <Button
                className="login-button font-size-button"
                onClick={this.handleClick}
              >
                Sign Up
              </Button>
              <div>Already have an account?  <Link to="/login">Login</Link></div>
            </Form.Field>
          </Form>
        </Grid.Column>
      </Grid>
    )
  }
}

export default connect(
  state => ({
    user: state.userRegister
  })
)(AppHOC(Signup));
