import React, { Component } from 'react';
import { Card, Icon, Image, Container, Grid, Modal, Checkbox } from 'semantic-ui-react'
import Header from '../../layouts/Header.jsx';
import SecondHeader from '../../layouts/SecondHeader.jsx';
import ClientFooter from '../../layouts/ClientFooter.jsx';
import './offers/offers.less';
import ViewDetails from './offers/ViewDetails.jsx';

class Offers extends Component {
  render() {
    return (
      <Container fluid>
	      <Header isClientSide={true} />
	      <SecondHeader />
	      <Grid className='offer-container'>
		      <Grid.Row style={{paddingTop: 0}}>
			      <Grid.Column width={4}>
						    <div style={{background: '#ffffff', boxShadow: '1px 3px 1px rgba(0, 0, 0, 0.25)', margin: '28px 20px 0 34px', padding: '36px 24px'}}>
							    <Grid.Row>
							      <Grid.Column> <Checkbox style={{fontSize: '24px', margin: '0px 0 0 0px'}} label={{ children: 'Area' }} /> </Grid.Column>
							    </Grid.Row>
							    <CheckboxComponent name={'Nagpur'} />
							    <Grid.Row>
							      <Grid.Column><Checkbox style={{fontSize: '24px', margin: '12px 0 0 21px'}} label={{ children: 'Mumbai' }} /></Grid.Column>
							    </Grid.Row>
							    <Grid.Row>
							      <Grid.Column><Checkbox style={{fontSize: '24px', margin: '12px 0 0 21px'}} label={{ children: 'Delhi' }} /></Grid.Column>
							    </Grid.Row>
							    <Grid.Row>
							      <Grid.Column><Checkbox style={{fontSize: '24px', margin: '12px 0 0 21px'}} label={{ children: 'Haydrabad' }} /></Grid.Column>
							    </Grid.Row>
						    </div>
						    <div style={{background: '#ffffff', boxShadow: '1px 3px 1px rgba(0, 0, 0, 0.25)', margin: '28px 20px 0 34px', padding: '36px 24px'}}>
							    <Grid.Row>
							      <Grid.Column> <Checkbox style={{fontSize: '24px', margin: '0px 0 0 0px'}} label={{ children: 'Venue' }} /> </Grid.Column>
							    </Grid.Row>
							    <Grid.Row>
							      <Grid.Column> <Checkbox style={{fontSize: '24px', margin: '12px 0 0 21px'}} label={{ children: 'Cricket' }} /> </Grid.Column>
							    </Grid.Row>
							    <Grid.Row>
							      <Grid.Column><Checkbox style={{fontSize: '24px', margin: '12px 0 0 21px'}} label={{ children: 'Football' }} /></Grid.Column>
							    </Grid.Row>
							    <Grid.Row>
							      <Grid.Column><Checkbox style={{fontSize: '24px', margin: '12px 0 0 21px'}} label={{ children: 'Hockey' }} /></Grid.Column>
							    </Grid.Row>
							    <Grid.Row>
							      <Grid.Column><Checkbox style={{fontSize: '24px', margin: '12px 0 0 21px'}} label={{ children: 'Badminton' }} /></Grid.Column>
							    </Grid.Row>
						    </div>
			      </Grid.Column>
			      <Grid.Column width={12}>
			        <h1 style={{color: '#F05327', margin: '35px 0 27px 0'}}>OFFERS</h1>
			        <Card.Group>
				        <Card style={{width: '457px', height: '600px'}}>
							    <Image style={{width: '280px', height: '428px', margin: '0 auto'}} src='../public/images/dashboard/offers.jpeg' />
							    <Card.Content style={{background: '#19191B'}}>
							      <Card.Header as='h1' style={{color: '#fff', fontSize: '35px', float: 'left', width: '100%'}}>30% off on Women Hockey</Card.Header>
							      <div style={{padding: '7px 29px', float: 'left', width: '100%'}}>
								      <div style={{textAlign: 'center', width: '42%', float: 'left'}}>
								      	<p style={{color: '#fff', fontSize: '18px'}}>Coupon Code</p>
								      	<div style={{background: '#EF9322', padding: '10px 22px', color: '#19191B', fontSize: '24px'}}>FTS30OFF</div>
								      </div>
								      <div style={{textAlign: 'right', float: 'right'}}>
								      	<p style={{color: '#fff', fontSize: '18px'}}>Valid till <span style={{color: '#EF9322', fontSize: '22px'}}>20/10/2018</span></p>
								      	<a style={{color: '#fff', fontSize: '24px', textAlign: 'right'}}>View Details</a>
								      </div>
								    </div>
							    </Card.Content>
							  </Card>
							  <Card style={{width: '457px', height: '600px'}}>
							    <Image style={{width: '280px', height: '428px', margin: '0 auto'}} src='../public/images/dashboard/offers.jpeg' />
							    <Card.Content style={{background: '#19191B'}}>
							      <Card.Header as='h1' style={{color: '#fff', fontSize: '35px', float: 'left', width: '100%'}}>30% off on Women Hockey</Card.Header>
							      <div style={{padding: '7px 29px', float: 'left', width: '100%'}}>
								      <div style={{textAlign: 'center', width: '42%', float: 'left'}}>
								      	<p style={{color: '#fff', fontSize: '18px'}}>Coupon Code</p>
								      	<div style={{background: '#EF9322', padding: '10px 22px', color: '#19191B', fontSize: '24px'}}>FTS30OFF</div>
								      </div>
								      <div style={{textAlign: 'right', float: 'right'}}>
								      	<p style={{color: '#fff', fontSize: '18px'}}>Valid till <span style={{color: '#EF9322', fontSize: '22px'}}>20/10/2018</span></p>
								      	<a style={{color: '#fff', fontSize: '24px', textAlign: 'right'}}>View Details</a>
								      </div>
								    </div>
							    </Card.Content>
							  </Card>
							  <Card style={{width: '457px', height: '600px'}}>
							    <Image style={{width: '280px', height: '428px', margin: '0 auto'}} src='../public/images/dashboard/offers.jpeg' />
							    <Card.Content style={{background: '#19191B'}}>
							      <Card.Header as='h1' style={{color: '#fff', fontSize: '35px', float: 'left', width: '100%'}}>30% off on Women Hockey</Card.Header>
							      <div style={{padding: '7px 29px', float: 'left', width: '100%'}}>
								      <div style={{textAlign: 'center', width: '42%', float: 'left'}}>
								      	<p style={{color: '#fff', fontSize: '18px'}}>Coupon Code</p>
								      	<div style={{background: '#EF9322', padding: '10px 22px', color: '#19191B', fontSize: '24px'}}>FTS30OFF</div>
								      </div>
								      <div style={{textAlign: 'right', float: 'right'}}>
								      	<p style={{color: '#fff', fontSize: '18px'}}>Valid till <span style={{color: '#EF9322', fontSize: '22px'}}>20/10/2018</span></p>
								      	<a style={{color: '#fff', fontSize: '24px', textAlign: 'right'}}>View Details</a>
								      </div>
								    </div>
							    </Card.Content>
							  </Card>
							  <Card style={{width: '457px', height: '600px'}}>
							    <Image style={{height: '428px', width: '280px', margin: '0 auto'}} src='../public/images/dashboard/offers.jpeg' />
							    <Card.Content style={{background: '#19191B'}}>
							      <Card.Header as='h1' style={{color: '#fff', fontSize: '35px', float: 'left', width: '100%'}}>30% off on Women Hockey</Card.Header>
							      <div style={{padding: '7px 29px', float: 'left', width: '100%'}}>
								      <div style={{textAlign: 'center', width: '42%', float: 'left'}}>
								      	<p style={{color: '#fff', fontSize: '18px'}}>Coupon Code</p>
								      	<div style={{background: '#EF9322', padding: '10px 22px', color: '#19191B', fontSize: '24px'}}>FTS30OFF</div>
								      </div>
								      <div style={{textAlign: 'right', float: 'right'}}>
								      	<p style={{color: '#fff', fontSize: '18px'}}>Valid till <span style={{color: '#EF9322', fontSize: '22px'}}>20/10/2018</span></p>
								      	<Modal
													trigger={<a style={{color: '#fff', fontSize: '24px', textAlign: 'right'}}>View Details</a>}
												>
												<ViewDetails />
												</Modal>
												{/* <a style={{color: '#fff', fontSize: '24px', textAlign: 'right'}}>View Details</a> */}
								      </div>
								    </div>
							    </Card.Content>
							  </Card>
						  </Card.Group>
			      </Grid.Column>
			    </Grid.Row>
		    </Grid>
	      <ClientFooter />
		  </Container>
    )
  }
}


export default Offers;

const CheckboxComponent = ({ name }) => {
	console.log(name);
	return (
		<Grid.Row>
			<Grid.Column>
				<Checkbox style={{fontSize: '24px', margin: '12px 0 0 21px'}} label={{ children: name }} />
			</Grid.Column>
		</Grid.Row>
	)
}