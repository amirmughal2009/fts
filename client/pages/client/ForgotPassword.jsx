import React, { Component } from 'react';
import { Grid, Icon, Header, Form, Button, Input, Message } from 'semantic-ui-react';
import './verify/verify.less';
import { connect } from 'react-redux';
import Actions from '../../actions/ForgotPasswordActions';
import AppHOC from '../../hoc/AppHOC.jsx';
import validator from 'validator';

class ForgotPassword extends Component{
  constructor(){
    super();
    this.state = {
      email: '',
      phoneNo: '',
      err:false,
      message:''
    }
  }

  handleClick = () => {
    const { email, phoneNo } = this.state;
    if(( !email || !validator.isEmail(email) ) && (!phoneNo)) {
        this.setState({ 
        err: true, 
        message: 'Please enter correct phone/email',
      });
    } else {
      const { dispatch, client } = this.props;
      dispatch(Actions.ForgotPasswordAction(client, this.state));
    }
  }

  handleChange = (key, e) => {
    this.setState({ 
      err: false, 
      [key]: e.target.value
    });
  }

  componentWillReceiveProps = (nextProps) => {
    const { forgotPassword } = nextProps;
    const { status, msg, data } = forgotPassword;
    if(status) {
      const { history } = this.props;
      history.push('/reset-password');
    }else{
      this.setState({
        err: true,
        message: msg
      });
    }
  }

  render(){
    return (
      <Grid textAlign="center" style={{ height: '100vh' }}>
        <Grid.Column style={{ width: '40vw' }} verticalAlign="middle">
          <Icon name='lock' size='huge' className="icon-color" />
          <Header as='h1' className="header-login-client">Forgot Password ?</Header>
          <br />  
          { this.state.err && 
            <Message
              error
              header='Forgot Password Error'
              content= { this.state.message }
            />
          }
          <Form>
            <div>
              We just need your registerd email address or
              phone number to send you password reset code.
            </div>
            { this.state.err && 
              <Message
                error
                header='Signup Error'
                content= { this.state.message }
              />
            }
            <br/>
            <Form.Group widths='equal'>
              <div style={{ float:"left", width: '30vw' }}> 
                <label className="disabled-label">Phone Number</label>
                <br/>
                <Input transparent 
                className="disabled-input margin-10" 
                onChange={(e) => this.handleChange('phoneNo', e)}
                />
              </div>              
              OR
              <div style={{ float:"right", width: '30vw' }}> 
                <label className="disabled-label">Email</label>
                <br/>
                <Input transparent 
                className="disabled-input margin-10" 
                onChange={(e) => this.handleChange('email', e)}
                />
              </div>              
             
            </Form.Group>
            <br/>
            <Form.Field>
              <div style={{ width: '50%' }}>
                <label className="disabled-label">Enter Code</label>
                <br/>
                <Input transparent className="disabled-input margin-10" />  
              </div>
            </Form.Field>
            
            <Form.Field>
              <Button
                className="login-button font-size-button"
                onClick={() => this.handleClick()}  
              >
                Verify Code
              </Button>
            </Form.Field>
          </Form>
        </Grid.Column>
      </Grid>
    )
  }
}

export default connect(
  state => ({
    forgotPassword: state.forgotPassword
  })
)(AppHOC(ForgotPassword));
