import React, { Component } from 'react';
import { Container, Grid, Header, Image, Menu, Button, List } from 'semantic-ui-react';
import './booknPlaydetails.less';

class ArenaTabs extends Component {
  state = { activeItem: 'facilities' }
  handleItemClick = (e, { name }) => this.setState({ activeItem: name })
  
  render() {
    const { activeItem } = this.state
    return (
      <div>
      <Container className='arena-tabs-holder'>
        <Menu pointing secondary fluid widths={6}>
          <Menu.Item 
            name='facilities' 
            active={activeItem === 'facilities'} 
            onClick={this.handleItemClick} 
          />
          <Menu.Item
            name='location'
            active={activeItem === 'location'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='reviews'
            active={activeItem === 'reviews'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='termsandConditions'
            active={activeItem === 'termsandConditions'}
            onClick={this.handleItemClick}
          />
        </Menu>
        <div className='facilities-holder'>
          <Header className='facility-heading' size='huge'>Facilities</Header>
          <Grid style={{padding: '60px 0 0 10%'}}>
            <ArenaFacilityImage image_src={'../public/images/booknplay/drops.svg'} name={'Water'} />
            <ArenaFacilityImage image_src={'../public/images/booknplay/shover.svg'} name={'Shower'} />
            <ArenaFacilityImage image_src={'../public/images/booknplay/forks.svg'} name={'Canteen'} />
            <ArenaFacilityImage image_src={'../public/images/booknplay/book.svg'} name={'Changing Room'} />
            <ArenaFacilityImage image_src={'../public/images/booknplay/cycle.svg'} name={'2 Wheeler Parking'} />
            <ArenaFacilityImage image_src={'../public/images/booknplay/orch.svg'} name={'4 Wheeler Parking'} />
            <ArenaFacilityImage image_src={'../public/images/booknplay/lamp.svg'} name={'Locker'} />
          </Grid>
          <Header className='facility-heading' size='huge'>Location</Header>
          <Image style={{}} src='../public/images/booknplay/map-image.png' />
          
          <Header className='facility-heading' size='huge'>Reviews</Header>
          <div className='review-item' style={{padding: '30px 0', borderBottom: '1px solid #19191B'}}>
            <div style={{width: '100%', float: 'left'}}>
              <p style={{float: 'left', fontSize: '25px'}}>Aashish Prajapat</p>
              <Button content='4.5' style={{background: '#FC9B04', color: '#fff', float: 'right'}} />
            </div>
            <p style={{color: '#565656', fontSize: '20px'}}>
              Mauris condimentum mollis mauris, tincidunt porttitor nisi aliquet lacinia. Vestibulum a est neque.
            </p>
            <div style={{background: '#EBEBEB', padding: '24px 30px'}}>
              <p style={{color: '#747478', fontSize: '20px'}}>
              Reply from Manager
              </p>
            </div>
          </div>
          <div className='review-item' style={{padding: '30px 0', borderBottom: '1px solid #19191B'}}>
            <div style={{width: '100%', float: 'left'}}>
              <p style={{float: 'left', fontSize: '25px'}}>Aashish Prajapat</p>
              <Button content='4.5' style={{background: '#FC9B04', color: '#fff', float: 'right'}} />
            </div>
            <p style={{color: '#565656', fontSize: '20px'}}>
              Mauris condimentum mollis mauris, tincidunt porttitor nisi aliquet lacinia. Vestibulum a est neque.
            </p>
            <div style={{background: '#EBEBEB', padding: '24px 30px'}}>
              <p style={{color: '#747478', fontSize: '20px'}}>
              Reply from Manager
              </p>
            </div>
          </div>
        </div>
      </Container>

      <Container className='terms'>
        <Header className='facility-heading' size='huge'>Terms and Conditions</Header>
        <List bulleted style={{fontSize: '20px', color: '#747478'}}>
          <List.Item style={{color: '#747478'}}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </List.Item>
          <List.Item style={{color: '#747478'}}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </List.Item>
          <List.Item style={{color: '#747478'}}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh . </List.Item>
      </List>
      </Container>
      </div>
    )
  }
}

export default ArenaTabs;

const ArenaFacilityImage = ({ image_src, name }) => {
	return (
		<Grid.Column width={2}>
      <Image style={{height: '90px', margin: '0 auto'}} src={image_src}/>
      <p style={{textAlign: 'center', color: '#747478', fontSize: '18px'}}>
        {name}
      </p>
    </Grid.Column>
	)
}


