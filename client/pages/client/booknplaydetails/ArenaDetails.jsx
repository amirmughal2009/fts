import React, { Component } from 'react';
import { Container, Grid, Header, Image, Rating } from 'semantic-ui-react';
import './booknPlaydetails.less';

class ArenaDetails extends Component {
  render() {
    return (
      <Container>
        <Header className='arena-header' as='h1' style={{color: '#F05327'}}> BOOK N PLAY</Header>
        <div className='arena-holder'>
          <Header as='h1'>Name of arena</Header>
          <p className='arena-text'>Akshya Nagar 1st Block 1st Cross, Rammurthy nagar, Bangalore-560016 </p>
          <div>
            <Image src='../public/images/booknplay/location-vector.png' avatar />
            <span><a href="#">Get Directions</a></span>
          </div>  
          <div className='gallary-text-holder'>
            <p className='gallary-text'>Gallary</p>
            <a className='photo-link' href="#">10 Photos</a>
          </div>
          <div>
            <Image.Group size='small'>
              <Image src='../public/images/booknplay/club1.png' />
              <Image src='../public/images/booknplay/club2.png' />
              <Image src='../public/images/booknplay/club3.png' />
              <Image src='../public/images/booknplay/club4.png' />
              <Image src='../public/images/booknplay/club5.png' />
            </Image.Group>
          </div>
          <div>
            <p style={{ color: '#FC9B04', margin: 0 }}>4.5</p>
						<Rating defaultRating={4} maxRating={5} />
          </div>
        </div>  
      </Container>
    )
  }
}
  
  export default ArenaDetails;