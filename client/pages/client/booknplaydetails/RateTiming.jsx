import React, { Component } from 'react';
import { Container, Grid, Header, Image, Rating } from 'semantic-ui-react';
import './booknPlaydetails.less';

class RateTimimg extends Component {
    render() {
      return (
        <Container>
            <Grid style={{margin: 0}}>
              <Grid.Row className='rate-time-holder' style={{textAlign: 'center'}}>
                <Grid.Column width={8} style={{borderRight: '1px solid #747478'}}>
                  <Header className='arena-rate' as='h1'>Rs. 200 / hour</Header>
                </Grid.Column>
                <Grid.Column width={8} className='timing-holder'>
                  <div className='time1'>
                    <h3>6:30am</h3>
                    <p>Opeining Time</p>
                  </div>
                  <div className='time2'>
                    <h3>6:30am</h3>
                    <p>Opeining Time</p>
                  </div>
                  <strong>S M T W T F S</strong>
                </Grid.Column>
              </Grid.Row>
            </Grid>
        </Container>
      )
    }
}

export default RateTimimg;