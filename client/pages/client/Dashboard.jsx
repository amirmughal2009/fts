import React, { Component } from 'react';
import Header from '../../layouts/Header.jsx';
import SecondHeader from '../../layouts/SecondHeader.jsx';
import NavigationHeader from '../../layouts/NavigationHeader.jsx';
import Slider from './dashboard/Slider.jsx';
import Quotes from './dashboard/Quotes.jsx';
import HowItWorks from './dashboard/HowItIworks.jsx';
import Tournaments from './dashboard/Tournaments.jsx'
import Offers from './dashboard/Offers.jsx';
import Footer from './dashboard/Footer.jsx';
import Win from './dashboard/WinMatches.jsx';
import Partner from './dashboard/Partner.jsx';
import ClientFooter from '../../layouts/ClientFooter.jsx';
import { Container, Grid, Image } from 'semantic-ui-react';
import './dashboard/dashboard.less';

class Dashboard extends Component {
  render() {
    return (
      <Container fluid className="home-container">
        <Header isClientSide={true} />
        <SecondHeader />
        <NavigationHeader />
        <Slider />
        <Quotes />
        <HowItWorks />
        <Tournaments />
        <div style={{position: 'relative'}}> 
          <Offers />
          <Win />
          <Image style={{position: 'absolute', right: '0', top: '360px', height: '570px'}} src='../public/images/dashboard/runningman.png' />
          <Partner />
        </div>
        <ClientFooter />
        <Footer />
      </Container>
    )
  }
}

export default Dashboard;
