import React, { Component } from 'react';
import { Container, Grid } from 'semantic-ui-react';
import Header from '../../layouts/Header.jsx';
import SecondHeader from '../../layouts/SecondHeader.jsx';
import ClientFooter from '../../layouts/ClientFooter.jsx';
import Footer from './dashboard/Footer.jsx';

class AboutUs extends Component {
	render() {
	  return (
			<Container fluid style={{background: '#C4C4C4'}}>
				<Header isClientSide={true} />
				<SecondHeader />
				<Container textAlign='left' >
					<div style={{textAlign: 'center', marginTop: '77px', paddingTop: '28px',textShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)', color: '#F05327', fontSize: '40px', fontWeight: '500', height: '230px', backgroundImage: 'url("../public/images/aboutus/about-us-main.png")', backgroundRepeat: 'no-repeat' ,backgroundSize: 'cover' }}>
					ABOUT US
					</div>
					<div style={{background: '#FFFFFF', padding: '100px 110px 40px', fontSize: '32px', color: '#565656'}}>
						<p style={{textAlign: 'center',color: '#19191B'}}>WHO WE ARE</p>
						<p>
						We are an online market place and aggregator for sports, fitness and various other physical activity service providers, to inspire people and help them to find, plan and start playing their favorite sport.
						</p>
						<br />
						<p>		
							Doing regularvphysical activity can make you feel good about yourself and can have various health benifits,
						</p>
						<Grid>
						<Grid.Row>
						<Grid.Column width={8}>
							<p>it can relieve stress and increase your productivity and concentration in your daily work, it can make you healther and a happy person with a fit body</p>
							<br />
							<p>Our aim is to encourage people to do some plysical activities regularly atleast one hour a day which will help them live good and healthy life.</p>
						</Grid.Column>
						<Grid.Column width={8}>
							<img 
								src='../public/images/aboutus/about-us-gif.png' size='small'
							/>
						</Grid.Column>
						</Grid.Row>
						</Grid>
					</div>
				</Container>
				<ClientFooter />
				<Footer />
			</Container>
	  )
	}
}
  
export default AboutUs;