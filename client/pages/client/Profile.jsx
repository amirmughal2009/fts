import React , { Component } from 'react';
import Header from '../../layouts/Header.jsx';
import SecondHeader from '../../layouts/SecondHeader.jsx';
import NavigationHeader from '../../layouts/NavigationHeader.jsx';
import { Grid, Image, Container, Button } from 'semantic-ui-react';

import './profile/profile.less';

class Profile extends Component {
  render() {
    return (
      <Container fluid>
        <Header isClientSide={true} />
        <SecondHeader />
        <NavigationHeader />
        <Container style={{borderBottom: '2px solid #C4C4C4'}}>
          <h1 className='profile-heading'>Profile</h1>
          <Grid centered columns={3} style={{ margin: '40px' }}>
            <Grid.Column style={{ textAlign: 'right', paddingTop: '9%' }}>
              <div className='profile-username'>USERNAME</div>
              <div className='profile-email'>useremailid@gmail.com</div><br/>
            </Grid.Column>
            <Grid.Column width={4} style={{ display:'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'center' }}>
              <div>
                <Image className='profile-image' src='../public/images/client/user.png' size='small' circular />
              </div>
              {/* <div className='profile-bookings-count'>52</div>
              <div className='profile-bookings'>Bookings</div> */}
            </Grid.Column>
            <Grid.Column style={{ textAlign: 'left', paddingTop: '9%' }}>
              <div style={{float: 'left'}}>
                <div className='fts-rank'>52</div>
                <div className='profile-email'>Your FTS Rank</div>
                <div className='minton'>
                  <Image src='../public/images/client/bedminton.svg' avatar />
                  <span>Badminton</span>
                </div>
              </div>
              <Button className="fts-btn">View Others</Button>
            </Grid.Column>
          </Grid>
          <Grid textAlign='center' columns={4} style={{ margin: '0 10%' }}>
            <BookingBlock />
            <BookingBlock />
            <BookingBlock />
            <BookingBlock />
          </Grid>
          <Grid textAlign='left' columns={2} style={{ margin: '0px 10% 0 5%', paddingTop: '8%' }}>
            <Grid.Column style={{  }}>
              <div className='profile-login-text'>Login Information</div>
            </Grid.Column>
            <Grid.Column textAlign='center' style={{  }}>
              <span className='profile-info-text'>Personal Information</span>
              <Image src='../public/images/client/check.svg' style={{display: 'inline-block', marginLeft: '20px'}} />              
            </Grid.Column>
          </Grid>
        </Container>
              
        <Container style={{borderBottom: '2px solid #C4C4C4', margin: '0 0 12px 0'}}>
          <Grid textAlign='left' divided columns={2} style={{ margin: '10px', paddingTop: '' }}>
            <Grid.Column style={{ padding: '2% 0% 5% 5%', display: 'flex', flexWrap: 'wrap', alignContent: 'space-between', height: '275px' }}>
              <LoginPersonalInfoBlock class_name={'login-email'} text1={'Email'} text2={'useremailid@gmail.com'} />  
              <LoginPersonalInfoBlock class_name={'change-link'} text1={'Password'} text2={'Change Password'} />  
              <LoginPersonalInfoBlock class_name={'login-email'} text1={'Phone Number'} text2={'9874563210'} />  
            </Grid.Column>
            <Grid.Column style={{ padding: '2% 0% 5% 5%', display: 'flex', flexWrap: 'wrap', alignContent: 'space-between', height: '275px' }}>
              <LoginPersonalInfoBlock class_name={'login-email'} text1={'Name'} text2={'User Name'} />  
              <LoginPersonalInfoBlock class_name={'login-email'} text1={'Date of birth'} text2={'12 / 02 / 2000'} />  
              <LoginPersonalInfoBlock class_name={'login-email'} text1={'Gender'} text2={'Male'} />          
            </Grid.Column>
          </Grid>    
        </Container>      


      </Container>
    )
  }
}

export default Profile;

const LoginPersonalInfoBlock = ( {class_name, text1, text2} ) => {
  return(
    <div className='login-info'>
      <div className='profile-email'>{ text1 }</div>
      <div className={ class_name }>{ text2 }</div>
    </div> 
  )
}

const BookingBlock = () => {
  return (
    <Grid.Column>
      <div className='profile-bookings-count'>05</div>
      <div className='profile-bookings'>Bookings</div>
    </Grid.Column>
  )
}