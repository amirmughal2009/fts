import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react'
import SidebarComponent from './SidebarComponent.jsx';

class Sidebar extends Component {
  render() {
    return (
      <div>
        <div style={{ width: '316px', background: '#ffffff', boxShadow: '1px 3px 1px rgba(0, 0, 0, 0.25)', margin: '96px 20px 0 34px', padding: '18px 24%', fontSize: '28px', color: '#F05327' }}>Filters</div>
        <SidebarComponent component_name={'City'} label1={'Nagpur'} label2={'Mumbai'} label3={'Delhi'} label4={'Hydrabad'} />
        <SidebarComponent component_name={'Area'} label1={'Football'} label2={'Cricket'} label3={'Delhi'} label4={'Hydrabad'} />
        <SidebarComponent component_name={'Sports'} label1={'Football'} label2={'Cricket'} label3={'Delhi'} label4={'Hydrabad'} />
        <SidebarComponent component_name={'Price'} label1={'below Rs. 500'} label2={'Rs. 500 - Rs. 1000'} label3={'Rs. 1000 - Rs. 2000'} label4={'Rs. 2000 and above'} />
      </div>
    )
  }
}

export default Sidebar;