import React, { Component } from 'react';
import { Grid, Segment, Checkbox } from 'semantic-ui-react'
import CheckboxComponent from '../offers/CheckboxComponent.jsx';

class SidebarComponent extends Component {
  render() {
    return (
      <div style={{ width: '316px', background: '#ffffff', boxShadow: '1px 3px 1px rgba(0, 0, 0, 0.25)', margin: '28px 20px 0 34px', padding: '36px 24px' }}>
        <h3>{this.props.component_name}</h3>
        <CheckboxComponent name={this.props.label1} />
        <CheckboxComponent name={this.props.label2} />
        <CheckboxComponent name={this.props.label3} />
        <CheckboxComponent name={this.props.label4} />
      </div>
    )
  }
}

export default SidebarComponent; 