import React, { Component } from 'react';
import { Menu, Icon, Dropdown, Image, Button, Grid, Modal, TextArea } from 'semantic-ui-react';

const trigger = (
  <span>
    <Image avatar src={'../public/images/logo/logo.png'} /><Icon disabled name='angle down' />
  </span>
)

const options = [
  { key: 'user', text: 'Account', icon: 'user' },
  { key: 'settings', text: 'Settings', icon: 'settings' },
  { key: 'sign-out', text: 'Sign Out', icon: 'sign out' },
]

class Header extends Component {
  constructor(){
    super();
    this.state = { openCity: false, openSport: false }
  }

  showCity = size => () => this.setState({ size, openCity: true })
  showSport = size => () => this.setState({ size, openSport: true })
  close = () => this.setState({ openCity: false, openSport:false })

  render() {
    const { openCity, size, openSport } = this.state;
    const { isClientSide } = this.props;
    
    return (
      <Menu fixed borderless attached={isClientSide ? 'true' : 'false' }>
        <Menu.Item style={{position: 'relative', marginRight: '10%', padding: 0}}>
          <img className='tri-image1' src='../public/images/logo/tri-header1.png' />
        </Menu.Item>
        <Menu.Item>
          <img className='logo-image' src='../public/images/logo/logo.png' />
        </Menu.Item>
        {
          isClientSide &&
          <Menu.Item>
            <Button className="home-header" onClick={this.showCity('mini')} >
              <span className='header-icon' style={{background: "url('./public/images/logo/city-icon.svg') no-repeat"}}></span>
              Select City
            </Button>
          </Menu.Item>  
        }
        {  isClientSide &&
          <Menu.Item>
            <Button className="home-header" onClick={this.showSport('small')} >
            <span className='header-icon' style={{background: "url('./public/images/logo/sports-icon.svg') no-repeat"}}></span>
              Select Sport
            </Button>
          </Menu.Item>  
        }
        {
          isClientSide &&
          <Menu.Menu position='right'>
            <Menu.Item className='site-phone' style={{fontSize: '18px'}}>
              <Icon name='phone' /> 03246203903
            </Menu.Item>
            <Menu.Item style={{fontSize: '18px', width: '50%'}}>
            <Grid>
              <Grid.Row className='social-btns' style={{paddingTop: 0, textAlign: 'right'}}>
                <Grid.Column>
                <Button circular color='facebook f' icon='facebook f' />
                <Button circular color='instagram' icon='instagram' />
                <Button circular color='twitter' icon='twitter' />
                <Button circular color='linkedin' icon='linkedin' />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row style={{paddingTop: 0, textAlign: 'right'}}>
                <Grid.Column>
                  <Icon name='mail' /> ftssupport@gmail.com
                </Grid.Column>
              </Grid.Row>
            </Grid>  
            </Menu.Item>
          </Menu.Menu>
        }
        {
          !isClientSide &&
          <Menu.Menu position='right'>
            <Menu.Item>
              <Icon name="chess rock" style={{ fontSize: '25px' }} />
            </Menu.Item>
            <Menu.Item>
              <Dropdown trigger={trigger} options={options} pointing='top left' icon={null} />
            </Menu.Item>
          </Menu.Menu>
        }
        <Modal 
          size={size} 
          open={openCity} 
          onClose={this.close}
          closeIcon>
          <Modal.Header style={{ 
            color:'#EF9322', 
            display:'flex', 
            justifyContent:'center' ,
            flexDirection: 'column',
            alignItems: 'center'
          }}>
            <div>
              <Icon size='big' name='building' style={{ color:'#EF9322', display:'flex', justifyContent:'center' }} />
            </div>
            Select City
          </Modal.Header>
          <Modal.Content>
            <TextArea style= {{ width:'100%' }}
								rows='5'></TextArea>
            <div style={{ display:'flex', justifyContent:'center' }}>
              <Button style={{ backgroundColor:'#EF9322' }}>
                Submit
              </Button>
            </div>    
          </Modal.Content>
        </Modal>

        <Modal 
          size={size} 
          open={openSport} 
          onClose={this.close}
          closeIcon>
          <Modal.Header style={{ 
            color:'#EF9322', 
            display:'flex', 
            justifyContent:'center', 
            flexDirection: 'column',
            alignItems: 'center' }}>
            <span className='header-icon' style={{
              background: "url('./public/images/logo/sports-icon.svg') no-repeat",
              backgroundColor:'#EF9322'
              }}></span>
            Select Sport
          </Modal.Header>
          <Modal.Content>
          <h5>All Sports</h5>
            <Grid columns={6} style={{ display:'flex', justifyContent:'center' }}>
              <Menu icon="labeled" borderless>
                <Menu.Item
                  name='cricket'
                  key='cricket'
                >
                  <Icon name='video camera' />
                  cricket
                </Menu.Item>
                <Menu.Item
                  name='baseball'
                  key='baseball'
                >
                  <Icon name='video camera' />
                  baseball
                </Menu.Item>
                <Menu.Item
                  name='football'
                  key='football'
                >
                  <Icon name='video camera' />
                  football
                </Menu.Item>
                <Menu.Item
                  name='basketball'
                  key='basketball'
                >
                  <Icon name='video camera' />
                  basketball
                </Menu.Item>
                <Menu.Item
                  name='hockey'
                  key='hockey'
                >
                  <Icon name='video camera' />
                  hockey
                </Menu.Item>
                <Menu.Item
                  name='boxing'
                  key='boxing'
                >
                  <Icon name='video camera' />
                  boxing
                </Menu.Item>
              </Menu>
            </Grid>
            <h5>Fitness</h5>
            <Grid columns={6} style={{ display:'flex', justifyContent:'center' }}>
              <Menu icon="labeled" borderless>
                <Menu.Item
                  name='cricket'
                  key='cricket'
                >
                  <Icon name='video camera' />
                  cricket
                </Menu.Item>
                <Menu.Item
                  name='baseball'
                  key='baseball'
                >
                  <Icon name='video camera' />
                  baseball
                </Menu.Item>
                <Menu.Item
                  name='football'
                  key='football'
                >
                  <Icon name='video camera' />
                  football
                </Menu.Item>
                <Menu.Item
                  name='basketball'
                  key='basketball'
                >
                  <Icon name='video camera' />
                  basketball
                </Menu.Item>
                <Menu.Item
                  name='hockey'
                  key='hockey'
                >
                  <Icon name='video camera' />
                  hockey
                </Menu.Item>
                <Menu.Item
                  name='boxing'
                  key='boxing'
                >
                  <Icon name='video camera' />
                  boxing
                </Menu.Item>
              </Menu>
            </Grid>
            <h5>Recreational Sports</h5>
            <Grid columns={6} style={{ display:'flex', justifyContent:'center' }}>
              <Menu icon="labeled" borderless>
                <Menu.Item
                  name='cricket'
                  key='cricket'
                >
                  <Icon name='video camera' />
                  cricket
                </Menu.Item>
                <Menu.Item
                  name='baseball'
                  key='baseball'
                >
                  <Icon name='video camera' />
                  baseball
                </Menu.Item>
                <Menu.Item
                  name='football'
                  key='football'
                >
                  <Icon name='video camera' />
                  football
                </Menu.Item>
                <Menu.Item
                  name='basketball'
                  key='basketball'
                >
                  <Icon name='video camera' />
                  basketball
                </Menu.Item>
                <Menu.Item
                  name='hockey'
                  key='hockey'
                >
                  <Icon name='video camera' />
                  hockey
                </Menu.Item>
                <Menu.Item
                  name='boxing'
                  key='boxing'
                >
                  <Icon name='video camera' />
                  boxing
                </Menu.Item>
              </Menu>
            </Grid>
            <div style={{ display:'flex', justifyContent:'center', marginTop:'30px' }}>
              <Button style={{ backgroundColor:'#EF9322' }}>
                Submit
              </Button>
            </div>
          </Modal.Content>
        </Modal>
      </Menu>
    )
  }
}

export default Header;
