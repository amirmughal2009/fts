import React, { Component } from 'react';
import { Menu, Container } from 'semantic-ui-react';
import './sidebar/sidebar.less';

class NavigationHeader extends Component {
	state = { activeItem: 'home' }
  handleItemClick = (e, { name }) => this.setState({ activeItem: name })
	
	render() {
		const { activeItem } = this.state
		return (
			
			<Container fluid style={{border: '3px solid #F05327'}}>
				<Menu className='nav-header' pointing secondary widths={8} style={{padding: '15px 0', borderBottom: 'none'}}>
					<Menu.Item 
						name='HOME' 
						active={activeItem === 'HOME'} 
						onClick={this.handleItemClick}
					/>
					<Menu.Item
							name='MATCHES'
							active={activeItem === 'MATCHES'}
							onClick={this.handleItemClick}
					/>
					<Menu.Item
							name='AUCTION'
							active={activeItem === 'AUCTION'}
							onClick={this.handleItemClick}
					/>
					<Menu.Item
							name='TOURNAMENT'
							active={activeItem === 'TOURNAMENT'}
							onClick={this.handleItemClick}
					/>
				</Menu>
			</Container>
		)
	}
}

export default NavigationHeader;