import React, { Component } from 'react';
import { Menu, Icon } from 'semantic-ui-react';
import './sidebar/sidebar.less';


class SideBar extends Component {

  state = { activeItem: 'DASHBOARD' }

  render() {
    const { activeItem } = this.state

    return (
      <Menu compact icon='labeled' vertical borderless>
        <MenuItem
          iconName={'tachometer alternate'}
          name={'DASHBOARD'}
          activeItem={'DASHBOARD'}
        />
        <MenuItem
          iconName={'user'}
          name={'CUSTOMER'}
        />
        <MenuItem
          iconName={'tasks'}
          name={'BOOKING'}
        />
        <MenuItem
          iconName={'quote right'}
          name={'QUOTES'}
        />
        <MenuItem
          iconName={'phone volume'}
          name={'CONTACTS'}
        />
        <MenuItem
          iconName={'bullhorn'}
          name={'MARKETING'}
        />
        <MenuItem
          iconName={'winner'}
          name={'REWARD POINTS'}
        />
      </Menu>
    )
  }
}

const MenuItem = ({ iconName, name, activeItem }) => {
  return (
    <Menu.Item
      name={iconName}
    >
      <Icon name={iconName} className={activeItem && 'active-color'}/>
      <div className={activeItem && 'active-color'}>
        {name}
      </div>
    </Menu.Item>
  )
}

export default SideBar;