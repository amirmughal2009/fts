import React, { Component } from 'react';
import { Grid, Container, Input, Button, Image, Icon } from 'semantic-ui-react';

class ClientFooter extends Component {
  render() {
    return (
      <Container fluid style={{ backgroundColor: '#19191B', padding: '40px 0' }}>
        <Container>      
          <Grid columns='equal'>
            <Grid.Column>
              <Image
                src='../public/images/logo/logo.png'
                size='small'
              /> <br />
              <div style={{ color: '#FFFFFF' }}>
                There are many variations of passages of Lorem Ipsum available,  but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, 
              </div>
            </Grid.Column>
            <Grid.Column style={{ color: '#FFFFFF', fontWeight: 'bold' }}>
              <a 
                href="/about-us" style={{ color: '#FFFFFF', fontWeight: 'bold', lineHeight: '30px'}}>
                About Us
              </a><br/>
              <a 
                href="/partner-with-us" style={{ color: '#FFFFFF', fontWeight: 'bold', lineHeight: '30px'}}>
                Partner With Us
              </a>
              <div style={{ lineHeight: '30px'}}>Reward Points</div>
              <div style={{ lineHeight: '30px'}}>Contact Us</div>
              <div style={{ lineHeight: '30px'}}>Terms and Conditions</div>
              <div style={{ lineHeight: '30px'}}>Policy</div>
            </Grid.Column>
            <Grid.Column style={{ color: '#FFFFFF', fontWeight: 'bold' }}>
              <div style={{ lineHeight: '30px'}}>
                <Icon name='mail' /> ftssupport@gmail.com
              </div>
              <div style={{ lineHeight: '30px'}}>
                <Icon name='phone' /> 03246203903
              </div>
              <div className='social-btns1'>
                <Button style={{background: '#fff', color: '#000'}} circular color='facebook f' icon='facebook f' />
                <Button style={{background: '#fff', color: '#000'}} circular color='instagram' icon='instagram' />
                <Button style={{background: '#fff', color: '#000'}} circular color='twitter' icon='twitter' />
                <Button style={{background: '#fff', color: '#000'}} circular color='linkedin' icon='linkedin' />
              </div>
              <div style={{paddingTop: '20px'}}>
                <Input placeholder='Your email' />
                <Button style={{marginLeft: '10px', borderRadius: 0, color: '#19191B', background: '#EF9322', border: '1px solid #F05327'}} content='Subscribe Now' />
              </div>
            </Grid.Column>
          </Grid>
        </Container>
      </Container>
    )
  }
}

export default ClientFooter;
