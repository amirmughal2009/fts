import React, { Component } from 'react';
import { Menu, Button, Icon, Image, Label } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import '../pages/client/dashboard/dashboard.less';

class SecondHeader extends Component {

  state = { activeItem: 'home' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {

    const { activeItem } = this.state

    return (
      
      <Menu inverted borderless attached>
        <Menu.Item style={{padding: '0 38px 19px 2px'}}>
          <img className='tri-image2' src='../public/images/logo/tri-header2.svg' />
        </Menu.Item>
        <Menu.Item style={{borderBottom: '1px solid #F05327'}}>
          <Image src='../public/images/logo/offers.svg' />            
          <span className='offer-text' style={{ fontSize: '18px' }}>Offers</span>
        </Menu.Item>
        <Menu.Item style={{borderBottom: '1px solid #F05327'}}>
          <Image src='../public/images/logo/Rectangle1.png' style={{ width: '85%' }}/>            
          <span className='offer-text'>
            <a style={{color: '#FFFFFF', fontSize: '18px'}} href="/partner-with-us">Partner With Us</a>
          </span>
        </Menu.Item>
        <Menu.Menu position='' style={{fontSize: '18px', paddingLeft: '22%', borderBottom: '1px solid #F05327'}}>
          <Menu.Item>
            <div>
              <div style={{ textAlign: 'center' }}>
                <Icon name='envelope' />
              </div>
              <div>Wallet</div>
            </div>
          </Menu.Item>
          <Menu.Item>
            <div>
              <div style={{ textAlign: 'center' }}>
                <Icon name='shopping cart' />
              </div>
              <div> Cart </div>
            </div>
          </Menu.Item>
          <Menu.Item>
            <Button className="second-header" >
              <NavLink to='/login' style={{textDecoration: 'none', color: '#EF9322', fontSize: '18px' }}>Login</NavLink>
            </Button>
            <Button className="second-header signup" >
              <NavLink to='/signup' style={{ textDecoration: 'none', color: '#19191B', fontSize: '18px' }}>SignUp</NavLink>
            </Button>
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    )
  }
}

export default SecondHeader;
