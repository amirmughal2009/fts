import gql from 'graphql-tag';

const USER_REGISTER = gql`
  mutation registerUser($gender: String!, $dateOfBirth: String!, $fullName: String!, $email: String!, $password: String!, $confirmPassword: String!, $phoneNo: String!) {
    registerUser(input: {fullName: $fullName, email: $email, password: $password, confirmPassword: $confirmPassword, phoneNo: $phoneNo, gender: $gender, dateOfBirth: $dateOfBirth }) {
      data {
        user {
          email
        }
      }
      status
      msg
    }
  }
`;

export default USER_REGISTER;
