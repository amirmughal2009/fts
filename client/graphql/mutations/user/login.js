import gql from 'graphql-tag';

const USER_LOGIN = gql`
  mutation loginUser($phoneNo: String!, $password: String!) {
    loginUser(phoneNo: $phoneNo, password: $password) {
      status
      msg
      data {
        user {
          phoneNo
          token
        }
      }
    }
  }
`;

export default USER_LOGIN;
