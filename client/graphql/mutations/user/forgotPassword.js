import gql from 'graphql-tag';

const FORGOT_PASSWORD = gql`
  mutation forgotPassword($phoneNo: String!, $email: String!) {
    forgotPassword(email: $email,phoneNo: $phoneNo) {
      status
      data {
        forgotType
      }
      msg
    }
  }
`;

export default FORGOT_PASSWORD;
