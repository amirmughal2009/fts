import gql from 'graphql-tag';

const RESET_PASSWORD = gql`
  mutation resetPassword($password: String!, $retypePassword: String!, $phoneNo: String!) {
    resetPassword(password: $password,retypePassword: $retypePassword,phoneNo: $phoneNo ) {
      status
      data {
        reset
      }
      msg
    }
  }
`;

export default RESET_PASSWORD;
