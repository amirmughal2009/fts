import React, { Component } from 'react';
import { ApolloProvider } from "react-apollo";
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';

const client = new ApolloClient({
  link: ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors)
        graphQLErrors.map(({ message, locations, path }) =>
          console.log(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
          ),
        );
      if (networkError) console.log(`[Network error]: ${networkError}`);
    }),
    // new HttpLink({
    //   uri: 'https://calm-tundra-30299.herokuapp.com/graphql',
    //   credentials: 'same-origin'
    // })
    new HttpLink({
      uri: 'http://localhost:5000/graphql',
      credentials: 'same-origin'
    })
  ]),
  cache: new InMemoryCache()
});

const AppHOC = (WrappedComponent) =>  {
  return class App extends Component {
    render() {
      return (
        <ApolloProvider client={client}>
          <WrappedComponent {...this.props} client={client}/>
        </ApolloProvider>
      )
    }
  }
}

export default AppHOC;