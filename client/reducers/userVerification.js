const initialState = {
  fetching: false,
  fetched: false,
  data: {},
  status: '',
  msg: ''
};

const userVerificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'USER_VERIFICATION': {
      return {
        ...state,
        fetching: true,
      };
    }
    case 'USER_VERIFICATION_FULFILLED': {
      return {
        ...state,
        fetching: false,
        fetched: true,
        ...action.payload,
      };
    }
    case 'USER_VERIFICATION_REJECTED': {
      return {
        ...state,
        fetching: false,
        fetched: true,
        error: action.payload
      };
    }
    default: {
      return state;
    }
  }
};

export default userVerificationReducer;
