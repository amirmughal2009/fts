const initialState = {
  fetching: false,
  fetched: false,
  data: {},
  status: '',
  msg: ''
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'USER_REGISTER': {
      return {
        ...state,
        fetching: true,
      };
    }
    case 'USER_REGISTER_REJECTED': {
      return {
        ...state,
        fetching: false,
        error: action.payload,
      };
    }
    case 'USER_REGISTER_FULFILLED': {
      const { registerUser } = action.payload;
      return {
        ...state,
        fetching: false,
        fetched: true,
        ...registerUser,
      };
    }
    default: {
      return state;
    }
  }
};

export default userReducer;
