const initialState = {
  fetching: false,
  fetched: false,
  error: null,
  data: {},
  status: '',
  msg: ''
};

const forgotPasswordReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'FORGOT_PASSWORD': {
      return {
        ...state,
        fetching: true
      };
    }
    case 'FORGOT_PASSWORD_REJECTED': {
      return {
        ...state,
        fetching: false,
        error: action.payload
      };
    }
    case 'FORGOT_PASSWORD_FULFILLED': {
      const { forgotPassword } = action.payload;
      return {
        ...state,
        fetching: false,
        fetched: true,
        ...forgotPassword
      };
    }
    default: {
      return state;
    }
  }
};

export default forgotPasswordReducer;