import { combineReducers } from 'redux';

import profile from './profileReducer';
import adminLogin from './adminLoginReducer';
import userRegister from './userRegisterReducer';
import userLogin from './userLoginReducer';
import forgotPassword from './forgotPasswordReducer';
import resetPassword from './resetPasswordReducer';
import userVerification from './userVerification';

export default combineReducers({
  profile,
  adminLogin,
  userRegister,
  userLogin,
  forgotPassword,
  resetPassword,
  userVerification
});
