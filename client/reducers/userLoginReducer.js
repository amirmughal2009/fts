const initialState = {
  fetching: false,
  fetched: false,
  error: null,
  data: {},
  status: '',
  msg: ''
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'USER_LOGIN': {
      return {
        ...state,
        fetching: true
      };
    }
    case 'USER_LOGIN_REJECTED': {
      return {
        ...state,
        fetching: false,
        error: action.payload
      };
    }
    case 'USER_LOGIN_FULFILLED': {
      const { loginUser } = action.payload;
      return {
        ...state,
        fetching: false,
        fetched: true,
        ...loginUser
      };
    }
    default: {
      return state;
    }
  }
};

export default userReducer;