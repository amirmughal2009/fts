const initialState = {
  fetching: false,
  fetched: false,
  error: null,
  email: null,
  password: null
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADMIN_LOGIN': {
      return {
        ...state,
        fetching: true
      };
    }
    case 'ADMIN_LOGIN_REJECTED': {
      return {
        ...state,
        fetching: false,
        error: action.payload
      };
    }
    case 'ADMIN_LOGIN_FULFILLED': {
      return {
        ...state,
        fetching: false,
        fetched: true,
        ...action.payload
      };
    }
    default: {
      return state;
    }
  }
};

export default userReducer;