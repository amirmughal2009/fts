const initialState = {
  fetching: false,
  fetched: false,
  error: null,
  data: {},
  status: '',
  msg: ''
};

const resetPasswordReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'RESET_PASSWORD': {
      return {
        ...state,
        fetching: true
      };
    }
    case 'RESET_PASSWORD_REJECTED': {
      return {
        ...state,
        fetching: false,
        error: action.payload
      };
    }
    case 'RESET_PASSWORD_FULFILLED': {
      const { resetPassword } = action.payload;
      return {
        ...state,
        fetching: false,
        fetched: true,
        ...resetPassword
      };
    }
    default: {
      return state;
    }
  }
};

export default resetPasswordReducer;