import React, { Component } from 'react';
import Header from '../../layouts/Header.jsx';
import SideBar from '../../layouts/SideBar.jsx';

import { Container, Grid } from 'semantic-ui-react';

class DashboardContainer extends Component {
  constructor() {
    super();
    this.state = {}
  }


  render() {
    const { children } = this.props;
    return (
      <div style={{ background: '#EFEDED' }}>
        <Header />
        <Container fluid>
          <Grid>
            <Grid.Column width={3}>
              <SideBar />
            </Grid.Column>
            <Grid.Column width={12}>
              {children}
            </Grid.Column>
          </Grid>
        </Container>
      </div>
    )
  }
}

export default DashboardContainer;
